#!/usr/bin/python

"""
analyze_rst_test_track_phase.py
  Analyzes and plots the information stored in the output file that is written
  by rst_test_track_phase.py. This ".out" file has a format as shown below:

  time      reset #    measured PPS skew     crtt   temperature
  16:53:06: Reset: 4: 6.57387738577927e-08 303324.6 22.1669857


-------------------------------------------------------------------------------
Copyright (C) 2024 Nikhef, Peter Jansweijer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------------------------------------------------------------------------------------------

Usage:
  analyze_rst_test_track_phase.py outputfile [-xadc xadc_temp_hh_mm_ss ]
  analyze_rst_test_track_phase.py -h | --help

Options:
  -h --help     Show this screen.
  outputfile	the filename(.out) to be analyzed
  -xadc         optional xadc_temp_hh_mm_ss file

"""

import os
import sys
import numpy
import pdb
from datetime import datetime

import matplotlib.pyplot as plt
plt.rcParams["lines.linewidth"] = 1

############################################################################

if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("outputfile", help="The filename(.out, written by rst_test_track_phase.py to be analyzed")
  parser.add_argument("-xadc", default= "", help="scan xadc_file for FPGA temperature sample at equal time")
  args = parser.parse_args()
  outfile = args.outputfile

  # Open data file for "read", in "binary" format
  # Then readline() returns type <bytes> that can be
  # decoded using "utf-8" into string
  data_file = open(outfile,"rb")

  xadc = False
  xadc_time_lst= []
  xadc_temp_lst= []
  if args.xadc != "":
    print("scan: " + args.xadc + " file for equal time FPGA XDAC temperature samples")

    try:
      xadc_file = open(args.xadc,"rb")
    except:
      print("XADC file not found: " + args.xadc)
      sys.exit()

    # Read xadc_temp_2yymmdd_hhmmdd.out file
    line = "non emtpy"
    while (line != ""):
      line = xadc_file.readline().decode("utf-8")
      line_lst = line.split(" ")
      if (len(line_lst) == 2):
        xadc_time_lst.append(line_lst[0])
        xadc_temp_lst.append(line_lst[1])

    xadc_arr = numpy.array([xadc_time_lst, xadc_temp_lst], float)
    xadc_file.close()
    xadc = True

  line_lst= []
  time_lst = []
  tic_lst = []
  crtt_lst = []
  temp_lst = []
  xadc_val_lst= []

  # Read RstTest_yymmdd_hhmmdd.out file
  line = "non emtpy"
  last_value = 0
  rst_number= 0
  while (line != ""):
    line = data_file.readline().decode("utf-8")
    line_lst = line.split(" ")
    if (len(line_lst) == 4):
      #time_lst.append(datetime.strptime(line_lst[0], "%H_%M_%S").time())
      time_lst.append(line_lst[0])
      tic_lst.append(line_lst[1])
      crtt_lst.append(line_lst[2])
      temp_lst.append(line_lst[3])

      if xadc:
        try:
          #pdb.set_trace()
          # find the index of the first occurence of the time in xadc_arr
          index_time_found = numpy.where(xadc_arr == float(line_lst[0]))[1][0]
          # read xadc temperature at that index
          last_value = xadc_arr[1,index_time_found]
        except:
          None
          print("### Warning: not XADC temperature found at " + line_lst[0] + ". Using last_value: " + str(last_value))
        xadc_val_lst.append(last_value)
      if (float(line_lst[1]) > 7.325e-8):
        print("outlier at " + str(rst_number) + ": " + line)
      rst_number += 1

  tic = numpy.array([range(len(tic_lst)), tic_lst], float)
  crtt = numpy.array([range(len(crtt_lst)), crtt_lst], float)
  temp = numpy.array([range(len(temp_lst)), temp_lst], float)
  tic_max=tic[1].max()
  tic_min=tic[1].min()
  tic_dif=tic_max - tic_min
  crtt_max=crtt[1].max()
  crtt_min=crtt[1].min()
  crtt_dif=crtt_max - crtt_min
  temp_max=temp[1].max()
  temp_min=temp[1].min()
  temp_dif=temp_max - temp_min

  tic_mean=tic[1].mean()
  tic_stdev=tic[1].std(ddof=1)

  if xadc:
    xadc_val = numpy.array([range(len(xadc_val_lst)), xadc_val_lst], float)
    xadc_max=xadc_val[1].max()
    xadc_min=xadc_val[1].min()
    xadc_dif=xadc_max - xadc_min

  data_file.close()

  # Plot: TIC + Temperature
  if xadc:
    fig, (ax1, ax2, ax3) = plt.subplots(3)
  else:
    fig, (ax1, ax2) = plt.subplots(2)
  fig.suptitle = outfile
  fig.tight_layout(pad = 2)

  lns = []
  ax1.set_title("Time Interval Count")
  ax1.set_ylabel("PPS Residual [s]", color = "blue")
  ax1.tick_params(axis = "y", labelcolor = "blue")
  ax1.text(0.98, 0.98, '$\Delta=%1.3e$' % (tic_dif), transform=ax1.transAxes, fontsize="small", verticalalignment='top', horizontalalignment='right')
  ax1.text(0.98, 0.90, '$\mu=%1.4e$' % (tic_mean), transform=ax1.transAxes, fontsize="small", verticalalignment='top', horizontalalignment='right')
  ax1.text(0.98, 0.82, '$\sigma=%1.3e$' % (tic_stdev), transform=ax1.transAxes, fontsize="small", verticalalignment='top', horizontalalignment='right')

  trace = ax1.plot(tic[0], tic[1]-tic_mean, "bo", markersize=1, label = "PPS Residual [s]" )
  lns = lns + trace
  ax_t = ax1.twinx()
  ax_t.set_ylabel("Temp [$^\circ$C]", color = "green")
  ax_t.tick_params(axis = "y", labelcolor = "green")
  trace = ax_t.plot(temp[0], temp[1], color = "green", label = "Temp [$^\circ$C]" )
  lns = lns + trace
  labels=[l.get_label() for l in lns]
  ax1.legend(lns, labels, loc="lower right", fontsize="small")

  lns = []
  ax2.set_title("Cable Round Trip Time")
  #ax2.set_xlabel("WR restart number")
  ax2.set_ylabel("CRTT [ps]", color = "blue")
  ax2.tick_params(axis = "y", labelcolor = "blue")
  ax2.text(0.98, 0.95, '$\Delta=%1.3e$' % (crtt_dif), transform=ax2.transAxes, fontsize="small", verticalalignment='top', horizontalalignment='right')
  trace = ax2.plot(crtt[0], crtt[1], "bo", markersize=1, label = "CRTT [ps]" )
  lns = lns + trace
  ax_t = ax2.twinx()
  ax_t.set_ylabel("Temp [$^\circ$C]", color = "green")
  ax_t.tick_params(axis = "y", labelcolor = "green")
  trace = ax_t.plot(temp[0], temp[1], color = "green", label = "Temp [$^\circ$C]" )
  lns = lns + trace
  labels=[l.get_label() for l in lns]
  ax2.legend(lns, labels, loc="lower right", fontsize="small")

  if xadc:
    lns = []
    ax3.set_title("FPGA Die Temperature via XADC")
    ax3.set_xlabel("WR restart number")
    ax3.set_ylabel("Die Temp [$^\circ$C]", color = "blue")
    ax3.tick_params(axis = "y", labelcolor = "blue")
    ax3.text(0.98, 0.95, '$min=%.1f$' % (xadc_min) + '; $max=%.1f$' % (xadc_max), transform=ax3.transAxes, fontsize="small", verticalalignment='top', horizontalalignment='right')
    trace = ax3.plot(xadc_val[0], xadc_val[1], "bo", markersize=1, label = "FPGA Die [$^\circ$C]" )
    lns = lns + trace
    ax_t = ax3.twinx()
    ax_t.set_ylabel("Temp [$^\circ$C]", color = "green")
    ax_t.tick_params(axis = "y", labelcolor = "green")
    trace = ax_t.plot(temp[0], temp[1], color = "green", label = "Temp [$^\circ$C]" )
    lns = lns + trace
    labels=[l.get_label() for l in lns]
    ax3.legend(lns, labels, loc = "lower right", fontsize="small")

  plt.tight_layout()
  plt.show()

  sys.exit()
