#!/usr/bin/python

"""
scan_file.py
  General purpose ".log" file scanner for mining and analyzing data.
-------------------------------------------------------------------------------
Copyright (C) 2024 Nikhef, Peter Jansweijer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------------------------------------------------------------------------------------------

Usage:
  scan_file.py logfile
  scan_file.py -h | --help

Options:
  -h --help     Show this screen.
  logfile	the filename(.log) to be analyzed

"""

import os
import sys
import numpy
import pdb
from datetime import datetime

import matplotlib.pyplot as plt
plt.rcParams["lines.linewidth"] = 1

############################################################################
##
## If run from commandline, we can test the library
##
"""
Usage:
  scan_file.py
  scan_file.py -h | --help

Options:
  -h --help   Show this screen.
"""

if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("logfile", help="The filename(.log, written by rst_test_track_phase.py to be analyzed")
  args = parser.parse_args()

  # Open data file for "read", in "binary" format
  # Then readline() returns type <bytes> that can be
  # decoded using "utf-8" into string
  try:
    data_file = open(args.logfile,"rb")
  except:
    print("log file not found: " + args.logfile)
    sys.exit()

  # Read xadc_temp_2yymmdd_hhmmdd.out file
  line = "non emtpy"
  while (line != ""):
    line = data_file.readline().decode("utf-8")
    if ("Reset:" in line):
      print(line)

  data_file.close()
  sys.exit()
