rem -----------------------------------------------------------------------------
rem Title      : Do Vivado merge elf into bit using mmi
rem -----------------------------------------------------------------------------
rem File       : do_vivado_mmi_elf.cmd
rem Author     : Peter Jansweijer <peterj@nikhef.nl>
rem Company    : Nikhef
rem Created    : 2024-04-05
rem Last update: 2024-04-05
rem Platform   : Vivado
rem Standard   : tcl
rem -----------------------------------------------------------------------------
rem  Description:
rem 
rem  $1	   sleeptime        Seconds between succesive samples (default 1)
rem
rem  output in a file called xadc_temp_ddmmyy_hhmmss
rem  Samples are taken until stopped by <ctlr>-c.
rem -----------------------------------------------------------------------------
rem 
rem  Copyright (c) 2024 Nikhef, Peter Jansweijer
rem
rem This source file is free software; you can redistribute it   
rem and/or modify it under the terms of the GNU Lesser General   
rem Public License as published by the Free Software Foundation; 
rem either version 2.1 of the License, or (at your option) any   
rem later version.                                               
rem
rem This source is distributed in the hope that it will be       
rem useful, but WITHOUT ANY WARRANTY; without even the implied   
rem warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
rem PURPOSE.  See the GNU Lesser General Public License for more 
rem details.                                                     
rem
rem You should have received a copy of the GNU Lesser General    
rem Public License along with this source; if not, download it   
rem from http://www.gnu.org/licenses/lgpl-2.1.html
rem
rem -----------------------------------------------------------------------------

@prompt $$$s

rem ### note that environment variable "VIVADO" must be set to something like "E:\Xilinx\Vivado\2017.1\bin\"
rem ### in your (User) Environment Variables
"%VIVADO%\vivado.bat" -mode batch -source sample_xadc_temp.tcl -tclargs %1

rem ### Cleanup old log files and stuff
del vivado*.jou
del vivado*.log
