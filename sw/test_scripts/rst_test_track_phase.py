#!/usr/bin/python

"""
RstTest_TrackPhase.py:
  Resets BabyWR using an Agilent 33600A Waveform Generator.
  The script initializes channel 1 to trigger a 100 ms negative pulse 1V5 => 0V,
  connected to the BabyWR reset (GPIO1V8_2), under software control.
  An Agilent 53230A Time Interval Counter is initialized to measure the skew
  between the (Grand-)Master PPS and BabyWR PPS.
  An Agilent 34461A Digital Multimer is initialzed to measure temperature using
  a PT100 (when tests are done in a climate chamber).
  USB connects to the terminal of the WRPC core in BabyWR.

  A first BabyWR Reset is triggered via the waveform generator.
  The terminal output is scanned for "t24p value" and once found the WR "stat on"
  command is given. Using the status output line the script waits for TRACK_PHASE,
  if so the mean value of 10 PPS skew Time Interval Counts, the mean value of 10
  crtt values and the temperature are measured and stored in an output file:
  "RstTest_%y%m%dT%H%M%S_%f.out" (and a log file containing the full WRPC terminal
  session). An example of the output format of the ".out" can be seen below:

  time     measured PPS skew    crtt     temperature
  16_53_06 6.57387738577927e-08 303324.6 22.1669857

  Measuring is performed until <ctrl-C>.

  Note 1: the serial interface may need "sudo"
  Note 2: the wrpc terminal shouldn't run "gui" when starting RstTest_TrackPhase.py
-------------------------------------------------------------------------------
Copyright (C) 2024 Nikhef, Jansweijer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
-------------------------------------------------------------------------------

Usage:
  RstTest_TrackPhase.py     [ -awg IP ] [ -tic IP ] [ -dmm IP ] [ -serial dev ]
  RstTest_TrackPhase.py     -h | --help

Options:
  -h --help    Show this screen.
  -awg         IP-Number of Waveform Generator Agilent 33600A, default: "10.0.0.234"
  -tic         IP-Number of Time Interval Counter Agilent 53230A, default: "10.0.0.32"
  -dmm         IP-Number of DMM Keysight 34461A, default: "10.0.0.237"
  -serial      BabyWR WRPC terminal serial port, default: "/dev/ttyUSB0"
"""

import os
import sys
import numpy
import serial
import time
import datetime
import pdb
import vxi11

# Add parent directory (containing 'lib') to the module search path
lib_path = (os.path.dirname(os.path.abspath(__file__)))
lib_path = os.path.join(lib_path,"..")
sys.path.insert(0,lib_path)

import lib.Agilent_33600A as Agilent33600
import lib.Keysight_53230A as TIC

###############################################
# Found a neat python timout function on:
# https://stackoverflow.com/questions/492519/timeout-on-a-function-call
# default returns None when timeout occurs
def timeout(func, args=(), kwargs={}, timeout_duration=1, default=None):
    import signal

    class TimeoutError(Exception):
        pass

    def handler(signum, frame):
        raise TimeoutError()

    # set the timeout handler
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_duration)
    try:
        result = func(*args, **kwargs)
    except TimeoutError as exc:
        result = default
    finally:
        signal.alarm(0)

    return result


############################################################################

def tic_init(counter):

  """
  Initialize the Time Interval Counter for PPS time difference measurement on channel 1 and 2.

    counter    -- instance of python-vxi connected to the Time Interval Counter
  """

  # Put the device in a known state
  counter.write("*RST")
  counter.write("*CLS")

  # Setup Time Interval measurement from channel 1 -> 2
  counter.write("CONFigure:TINTerval (@1),(@2)")
  counter.write("SENSe:GATE:STARt:DELAy:SOURce IMMediate")
  counter.write("SENSE:TINTerval:GATE:SOURce IMMediate")
  counter.write("SENSE:TINTerval:GATE:PTRGOLarity POSitive")

  # Use external 10MHz reference
  #counter.write("ROSCillator:SOURce EXTernal")
  # Use internal 10MHz reference
  counter.write("ROSCillator:SOURce INTernal")

  # Disable timeout (otherwise we might loose measurements)
  counter.write("SYSTem:TIMeout INFinity")

  # Channel 1 setup (BabyWr PPS LVDS):
  counter.write("INP1:COUP DC")
  counter.write("INP1:RANGE 5")
  counter.write("INP1:SLOPE POS")
  counter.write("INP1:LEVEL:AUTO OFF")
  counter.write("INP1:IMP 50")
  counter.write("INP1:LEV 0.9")

  # Channel 2 setup (WR Switch PPS):
  counter.write("INP2:COUP DC")
  counter.write("INP2:RANGE 5")
  counter.write("INP2:SLOPE POS")
  counter.write("INP2:LEVEL:AUTO OFF")
  counter.write("INP2:IMP 50")
  counter.write("INP2:LEV 1.4")

  return


###############################################
def wr2wrpc(ser, cmd, prompt="=>"):

  ser.write(bytes("\r", 'utf-8'))
  time.sleep (0.1)
  ser.flushInput()

  for i in range(len(cmd)):
    ser.write(bytes(cmd[i], 'utf-8'))
    time.sleep (0.1)
  time.sleep (0.5)
  #ser.readline().decode('utf-8')	# Readback command

  #print(prompt + cmd)

  return


###############################################
def get_statusline(ser_slave):

  stat_lst = []
  while len(stat_lst) < 27:             # Keep reading until
    stat = ser_slave.readline().decode('utf-8')         # Readback valid status line
    stat_lst = stat.split(' ')          # split on spaces

  return(stat_lst)


###############################################
def wait_for_track_phase(ser):

  track_phase = False

  while not track_phase:	# then wait for "SYNC_PHASE" state
    stat = ser.readline().decode('utf-8')	# Readback status line
    stat_lst = stat.split(' ')	# split on spaces
    if not (stat_lst == None or len(stat_lst) < 25):
      print(stat_lst[6])
      if "TRACK_PHASE" in stat_lst[6]:
        track_phase = True
        print (stat)

  return(track_phase)


###############################################
def get_crtt(ser):

  success = False
  while not success:
    stat = ser.readline().decode('utf-8')	# Readback status line
    stat_lst = stat.split(' ')	        # split on spaces
    print(stat)
    if not (stat_lst == None or len(stat_lst) < 17):
      print(stat_lst[16])
      crtt = stat_lst[16].split(':')[1]     # split on ":" and return second item
      success = True

  return(crtt)


###############################################
# Main
###############################################

"""
Usage:
  RstTest_TrackPhase.py     [ -awg IP ] [ -tic IP ] [ -dmm IP ] [ -serial dev ]
  RstTest_TrackPhase.py     -h | --help

Options:
  -h --help    Show this screen.
  -awg         IP-Number of Waveform Generator Agilent 33600A, default: "10.0.0.234"
  -tic         IP-Number of Time Interval Counter Agilent 53230A, default: "10.0.0.32"
  -dmm         IP-Number of DMM Keysight 34461A, default: "10.0.0.237"
  -serial      BabyWR WRPC terminal serial port, default: "/dev/ttyUSB0"
"""

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("-awg", default="10.0.0.234", help="IP-Number of Waveform Generator Agilent 33600A")
  parser.add_argument("-tic", default="10.0.0.32", help="IP-Number of Time Interval Counter Agilent 53230A")
  parser.add_argument("-serial", default="/dev/ttyUSB0")
  parser.add_argument("-dmm", default="10.0.0.237", help="IP-Number of DMM Keysight 34461A")
  args = parser.parse_args()
  print("Use Serial port: ",args.serial)

  wfg =  vxi11.Instrument(args.awg)
  print(wfg.ask("*IDN?"))
  # Returns 'Agilent Technologies,33622A,MY53800427,A.01.10-2.25-03-64-02'

  # configure the Wavefrom Generator for triggered resets
  Agilent33600.wfg_trigger_reset(wfg)

  counter =  vxi11.Instrument(args.tic)
  print(counter.ask("*IDN?"))
  # Returns 'Agilent Technologies,53230A,MY50001484,02.05-1519.666-1.19-4.15-127-155-35'

  # Initialize Time Interval Counter (1-2)
  # Channel 1 setup (BabyWr PPS LVDS, 50 Ohm, Level 0.9):
  # Channel 2 setup (WR Switch PPS, 50 Ohm, Level 1.4):
  tic_init(counter)

  if args.dmm != None:      # open and configure Digital Multimeter with attached PT100
    dmm =  vxi11.Instrument(args.dmm)
    print(dmm.ask("*IDN?"))
    # Returns 'Keysight Technologies,34461A,MY57227450,A.03.00-02.40-03.00-00.52-04-02'

    # Configure for 4 wire temperature measurement
    dmm.write("SAMPle:COUNt 1")
    dmm.write("UNIT:TEMPerature C")
    print("Current temperature", float(dmm.ask("MEAS:TEMP? FRTD")))

  # Initialize the serial terminal to BabyWR
  ser = serial.Serial()
  ser.port = args.serial
  ser.baudrate = 115200
  ser.parity = serial.PARITY_NONE
  ser.bytesize = serial.EIGHTBITS
  ser.stopbits = serial.STOPBITS_ONE
  ser.timeout = None
  ser.open()

  timestamp = datetime.datetime.now()  #micro seconds timing
  timestr = timestamp.strftime("%y%m%d_%H%M%S")

  filename="RstTest_" + timestamp.strftime("%y%m%d_%H%M%S")
  outfile = open(filename + ".out","w")
  logfile = open(filename + ".log","w")
  wfg.write("*TRG")
  Resets = 1
  print("===> Reset: " + str(Resets) + " at: " + timestr)
  outfile.write(timestr + " ")
  logfile.write("===> Reset: " + str(Resets) + " at: " + timestr + "\n")

  while 1:
    line = ""
    track_phase = False

    while not track_phase:
      while not "wrc#" in line:
        line = ser.readline().decode('utf-8')
        print(line)
        logfile.write(line)

      wr2wrpc(ser,"stat on\r")
      # Call "wait_for_track_phase(ser_slave)" with a timeout of 180 seconds
      # If no TRACK_PHASE within 60 seconds then try restart link!
      # track_phase = run_with_limited_time(wait_for_track_phase, (ser_slave, ), {}, 180.0)
      # if not track_phase:
      track_phase = timeout(wait_for_track_phase, (ser, ), {}, 3600)
      if track_phase == None:
        timestamp = datetime.datetime.now()  #micro seconds timing
        timestr = timestamp.strftime("%y%m%d_%H%M%S ")
        print (timestr + "### Timeout waiting for TRACK_PHASE; Force Reset\n")
        logfile.write(timestr + "### Timeout waiting for TRACK_PHASE; Force Reset\n")
        # Reset
        Resets = Resets + 1
        timestamp = datetime.datetime.now()  # micro seconds timing
        timestr = timestamp.strftime("%y%m%d_%H%M%S")
        print("===> Reset: " + str(Resets) + " at: " + timestr)
        outfile.write(timestr + " ")
        logfile.write("===> Reset: " + str(Resets) + " at: " + timestr + "\n")
        '''
        ser.close()
        outfile.close()
        logfile.close()
        sys.exit()
        '''
      else:
        # Take 10 PPS TIC measurements and write the mean value to file
        meas_lst = []
        crtt_lst = []

        for i in range(10):
          meas_lst.append(float(counter.ask("READ?")))
          crtt_lst.append(int(get_crtt(ser)))

        meas_arr = numpy.array(meas_lst)
        meas_mean = meas_arr.mean()
        crtt_arr = numpy.array(crtt_lst)
        crtt_mean = crtt_arr.mean()
        # with current PPS cable (via LeCroy channel 1) delay ~ 81.93 ns)
        '''
        if meas_mean > 83e-9:
          print("TIC Outlier: ", meas_arr)
          logfile.write("TIC Outlier: ", meas_arr)
        '''
        if args.dmm != None:
          temp_str = str(float(dmm.ask("MEAS:TEMP? FRTD")))
        else:
          temp_str = ""
        outfile.write(str(meas_mean) + " " + str(crtt_mean) + " " + temp_str + "\n")
        outfile.flush()

        print("TIC: " + str(meas_mean))
        print("CRTT: " + str(crtt_mean))
        print("Temp: " + temp_str)
        logfile.write("TIC: " + str(meas_mean) + "\n")
        logfile.write("CRTT: " + str(crtt_mean) + "\n")
        logfile.write("Temp: " + temp_str + "\n")
        logfile.flush()

        # Reset again
        Resets = Resets + 1
        timestamp = datetime.datetime.now()  # micro seconds timing
        timestr = timestamp.strftime("%y%m%d_%H%M%S")
        print("===> Reset: " + str(Resets) + " at: " + timestr)
        outfile.write(timestr + " ")
        logfile.write("===> Reset: " + str(Resets) + " at: " + timestr + "\n")

      wfg.write("*TRG")
      time.sleep (0.5)
      wr2wrpc(ser,"stat off\r")

  ser.close()
  outfile.close()
  logfile.close()

