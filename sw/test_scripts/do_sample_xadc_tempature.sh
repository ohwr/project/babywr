#! /bin/bash

#-----------------------------------------------------------------------------
# Title      : Sample XADC Temperature values and store them in a file
#-----------------------------------------------------------------------------
# File       : sample_xadc_temp.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2024-04-05
# Last update: 2024-04-05
# Platform   : Vivado
# Standard   : tcl
#-----------------------------------------------------------------------------
# Description:
#
# $1	sleeptime        Seconds between succesive samples (default 1)
#
#  output in a file called xadc_temp_ddmmyy_hhmmss
#  Samples are taken until stopped by <ctlr>-c.
#-----------------------------------------------------------------------------
#
# Copyright (c) 2024 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

vivado -mode batch -source sample_xadc_temp.tcl -tclargs $1

# Cleanup old log files and stuff
rm vivado*.jou 2>/dev/null
rm vivado*.log 2>/dev/null
