#-----------------------------------------------------------------------------
# Title      : Sample XADC Temperature values and store them in a file
#-----------------------------------------------------------------------------
# File       : sample_xadc_temp.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2024-04-05
# Last update: 2024-04-08
# Platform   : Vivado
# Standard   : tcl
#-----------------------------------------------------------------------------
# Description:
#
# $1	sleeptime        Seconds between succesive samples (default 1)
#
# Output:
# a file "xadc_temp_yymmdd_hhmmss.out" with <space> separated format:
#                yymmdd_hhmmss temp
#                240408_154123 59.3
# Samples are taken until stopped by <ctlr>-c.
#-----------------------------------------------------------------------------
#
# Copyright (c) 2024 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

if { $argc == 1 } {
  set sleeptime [lindex $argv 0]
} else {
  puts "sample_xadc_temp.tcl expects one number."
  puts "Now using default:"
  puts "   sleeptime        1 sample per second"
  set sleeptime 1
  set hw_device "xcau10p_0"
}

puts $sleeptime

open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target
current_hw_device [get_hw_devices $hw_device]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices $hw_device] 0]

set timestamp [clock format [clock seconds] -format "%y%m%d_%H%M%S"]
set fd [open "./xadc_temp_$timestamp.out" "w"]

while {true} {
  set timestamp [clock format [clock seconds] -format "%y%m%d_%H%M%S"]
  refresh_hw_sysmon [get_hw_sysmons localhost:3121/xilinx_tcf/Digilent/210249B2C234/$hw_device/SYSMON] 
  set temp [get_property TEMPERATURE [get_hw_sysmons localhost:3121/xilinx_tcf/Digilent/210249B2C234/$hw_device/SYSMON]]
  puts $fd "$timestamp $temp"
  flush $fd
  puts "$timestamp $temp"
  exec sleep $sleeptime
}

close $fd
