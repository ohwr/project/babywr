#-----------------------------------------------------------------------------
# Title      : Vidado Do Implement
#-----------------------------------------------------------------------------
# File       : viv_do_impl.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2018-01-24
# Last update: 2021-06-01
# Platform   : FPGA-generics
# Standard   : VHDL
#-----------------------------------------------------------------------------
# Description:
#
# Implement the design
#-----------------------------------------------------------------------------
#
# Copyright (c) 2021 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

puts "implement the design"
# Insert the command to launch implementation run here 
launch_runs -verbose impl_1

puts "wait until implementation done"
# Insert the command to wait on impl_1 run here 
wait_on_run impl_1

puts "generate bit file"
launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1

puts "generate bit file completed"

# verbose is needed for open_run (Windows10 issue?) otherwise the next
# report statement is already executed before the design is loaded in memory...
open_run -verbose impl_1
report_drc -file drc.log
report_utilization -file utilization.log
report_clocks -file clock.log
report_timing_summary -file timing.log