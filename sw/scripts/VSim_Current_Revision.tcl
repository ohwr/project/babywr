#-----------------------------------------------------------------------------
# Title      : Set Current Revision
#-----------------------------------------------------------------------------
# File       : VSim_Current_Revision.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2018-01-24
# Last update: 2021-06-01
# Platform   : FPGA-generics
# Standard   : VHDL
#-----------------------------------------------------------------------------
# Description:
#
# Set and Get the current Date and Revision number
#-----------------------------------------------------------------------------
#
# Copyright (c) 2021 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------
# VSim_Current_Revision.tcl

puts "current year = [set current_year [scan [clock format [clock seconds] -format %y] %d]]"
set current_year_h [expr ($current_year / 10)]
set current_year_l [expr ($current_year - 10 * $current_year_h)]
puts "current month = [set current_month [scan [clock format [clock seconds] -format %m] %d]]"
set current_month_h [expr ($current_month / 10)]
set current_month_l [expr ($current_month - 10 * $current_month_h)]
puts "current day = [set current_day [scan [clock format [clock seconds] -format %d] %d]]"
set current_day_h [expr ($current_day / 10)]
set current_day_l [expr ($current_day - 10 * $current_day_h)]
set current_date [expr (1048576*$current_year_h + 65536*$current_year_l + 4096*$current_month_h + 256*$current_month_l + 16*$current_day_h + $current_day_l)]

puts "current revision = [set current_revision 1]"

