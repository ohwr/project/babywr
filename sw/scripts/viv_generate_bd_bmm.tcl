#-----------------------------------------------------------------------------
# Title      : Vidado Generate _bd.bmm File
#-----------------------------------------------------------------------------
# File       : viv_generate_bd_bmm.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2018-01-24
# Last update: 2021-06-01
# Platform   : FPGA-generics
# Standard   : VHDL
#-----------------------------------------------------------------------------
# Description:
#
# Generates a Block Memory Mapping (BMM) file
#-----------------------------------------------------------------------------
#
# Copyright (c) 2021 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

#open_run impl_2
set BB FALSE

#	foreach fn $project_bmm_files {
#   set temp [file rootname $fn]
#		set bd_fn ${temp}_bd.bmm
		set fn $bmm_file
    set bd_fn $bmm_bd
		puts "INFO: Creating file $bd_fn"		
		set fileID [open $fn r]

		if [catch {open $bd_fn w} fileID_bd] {
			puts "ERROR: Could not open file $bd_fn"
			close $fileID
			exit
		}
		
		while {[gets $fileID line] >= 0 } {	
			#puts "DEBUG: BB is $BB"
			#puts "DEBUG: Line is $line"
			if {[regexp {^[ \t]+BUS_BLOCK} $line]} {
				set BB TRUE
				puts $fileID_bd $line 
				continue
			} elseif {[regexp {^[ \t]+END_BUS_BLOCK;} $line]} {
				set BB FALSE
			}
			
			if { $BB eq "TRUE" } {
				if {[regexp {(^[ \t]+([^\s]*).*);} $line match new_line inst] == 1} {
					set BRAM_site [get_property SITE [get_cells $inst]]
					set expression [regexp {_(.*)} $BRAM_site match bmm_string]
					#puts "INFO: Located BRAM at site $BRAM_site"
					set new_line "$new_line PLACED = ${bmm_string};"
					#puts $new_line
					puts $fileID_bd $new_line
				} else {
				#puts "ERROR: Script not expecting this input. Please contact Xilinx"
				}
			} else {
				puts $fileID_bd $line 
			}
		}
		
		
		close $fileID_bd
		close $fileID
#	}



