rem -----------------------------------------------------------------------------
rem Title      : Do Vivado merge elf into bit using mmi
rem -----------------------------------------------------------------------------
rem File       : do_vivado_mmi_elf.cmd
rem Author     : Peter Jansweijer <peterj@nikhef.nl>
rem Company    : Nikhef
rem Created    : 2020-07-02
rem Last update: 2022-11-14
rem Platform   : FPGA-generics
rem Standard   : VHDL
rem -----------------------------------------------------------------------------
rem Description:
rem
rem This script merge elf into bit using mmi
rem -----------------------------------------------------------------------------
rem
rem Copyright (c) 2021 Nikhef, Peter Jansweijer
rem
rem This source file is free software; you can redistribute it   
rem and/or modify it under the terms of the GNU Lesser General   
rem Public License as published by the Free Software Foundation; 
rem either version 2.1 of the License, or (at your option) any   
rem later version.                                               
rem
rem This source is distributed in the hope that it will be       
rem useful, but WITHOUT ANY WARRANTY; without even the implied   
rem warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
rem PURPOSE.  See the GNU Lesser General Public License for more 
rem details.                                                     
rem
rem You should have received a copy of the GNU Lesser General    
rem Public License along with this source; if not, download it   
rem from http://www.gnu.org/licenses/lgpl-2.1.html
rem
rem -----------------------------------------------------------------------------

@prompt $$$s

set proj_name=%~n1%
set wrpc_cpu_mmi=%proj_name%.mmi
set wrpc_cpu_elf=%2%
set wrpc_cpu_instpath="wrpc_cpu_memory"

if not exist "%1%" (
  @echo ### %1% ### bit file not found
  @echo Usage: do_vivado_mmi_elf <bitfile>.bit <elffile>.elf
  exit /B
)
if not "%~x1%"==".bit" (
  @echo ### %1% ### bit file not found
  @echo Usage: do_vivado_mmi_elf <bitfile>.bit <elffile>.elf
  exit /B
)

if not exist "%2%" (
  @echo ### %2% ### elf file not found
  @echo Usage: do_vivado_mmi_elf <bitfile>.bit <elffile>.elf
  exit /B
)
if not "%~x2%"==".elf" (
  @echo ### %2% ### elf file not found
  @echo Usage: do_vivado_mmi_elf <bitfile>.bit <elffile>.elf
  exit /B
)

if not exist "%wrpc_cpu_mmi%" (
  @echo ### %wrpc_cpu_mmi% ### mmi file not found, has it been generated?"
  exit /B
)
rem ### Cleanup old log files and stuff
del vivado_mmi_elf.log
del updatemem*.jou
del updatemem*.log

rem ### note that environment variable "VIVADO" must be set to something like "E:\Xilinx\Vivado\2017.1\bin\"
rem ### in your (User) Environment Variables
%VIVADO%updatemem -meminfo %proj_name%.mmi -data %wrpc_cpu_elf% -bit %proj_name%.bit -proc %wrpc_cpu_instpath% -out %proj_name%_elf.bit -force >> vivado_mmi_elf.log

