#-----------------------------------------------------------------------------
# Title      : Vidado Generate BIN and MCS files
#-----------------------------------------------------------------------------
# File       : viv_gen_bin_mcs.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2018-01-24
# Last update: 2021-06-01
# Platform   : FPGA-generics
# Standard   : VHDL
#-----------------------------------------------------------------------------
# Description:
#
# Generates BIN and MCS files
#-----------------------------------------------------------------------------
#
# Copyright (c) 2021 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

# set proj_name and proj_dir (without updating the revision!):
set argv [list no_update_revision ]
set argc 2
source proj_properties.tcl

puts "generate bin and mcs files"
#open_run impl_1
write_cfgmem -force -format mcs -interface spix4 -size 128 -loadbit {up 0x0 fpga_elf.bit} -file fpga_elf.mcs
write_cfgmem -force -format bin -size 512 -interface spix4 -loadbit {up 0x0 fpga_elf.bit} -file fpga_elf.bin
