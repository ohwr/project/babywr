#!/usr/bin/python

"""
#-----------------------------------------------------------------------------
# Title      : Vidado Do All
#-----------------------------------------------------------------------------
# File       : conv_file_list_to_xilinx.py
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2018-01-24
# Last update: 2021-06-01
# Platform   : FPGA-generics
# Standard   : VHDL
#-----------------------------------------------------------------------------
# Description:
# 
# Converts the file that lists al files needed in the project to Xilinx ".prj" and 
# ".ise" files. The input file list is usually a concatenation of the file list that
# is generated using: "hdlmake list-files > hdlmake_list" together with a list
# of private project files.
#-----------------------------------------------------------------------------
#
# Copyright (c) 2021 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

Usage:
  conv_file_list_to_xilinx.py
  conv_file_list_to_xilinx.py     -h | --help

  <name>     name of the file that lists al files needed in the project.
             This is usually a concatenation of the file list that is generated
             using: "hdlmake list-files > hdlmake_list" together with a list
             of CLB project files.
  -o <name>  optional design name, default: "fpga".
             outputs Xilinx files <name>.prj and <name>.ise

Options:
  -h --help    Show this screen.

"""

import os
import sys
import datetime

import pdb

############################################################################

def prj_add_verilog(prj_file, line):
  prj_file.write("verilog work " + line + "\"\n")
  return()

def ise_add_verilog(ise_file, line):
  ise_file.write("    <file xil_pn:name=" + line + "\" xil_pn:type=\"FILE_VERILOG\">\n")
  ise_file.write("      <association xil_pn:name=\"BehavioralSimulation\"/>\n")
  ise_file.write("      <association xil_pn:name=\"Implementation\"/>\n")
  ise_file.write("    </file>\n")
  return()

def prj_add_vhdl(prj_file, line):
  prj_file.write("vhdl work " + line + "\"\n")
  return()

def ise_add_vhdl(ise_file, line):
  ise_file.write("    <file xil_pn:name=" + line + "\" xil_pn:type=\"FILE_VHDL\">\n")
  ise_file.write("      <association xil_pn:name=\"BehavioralSimulation\"/>\n")
  ise_file.write("      <association xil_pn:name=\"Implementation\"/>\n")
  ise_file.write("    </file>\n")
  return()

############################################################################
#
# If run from command line, we can test the library
#
"""
Usage:
  conv_file_list_to_xilinx.py
  conv_file_list_to_xilinx.py     -h | --help

  <name>     name of the file that lists al files needed in the project.
             This is usually a concatenation of the file list that is generated
             using: "hdlmake list-files > hdlmake_list" together with a list
             of CLB project files.
  -o <name>  optional design name, default: "fpga".
             outputs Xilinx files <name>.prj and <name>.ise

Options:
  -h --help    Show this screen.

"""
if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("name", help="file list, output by: hdlmake list-files")
  parser.add_argument("-oname", default="do_input_file_list.cmd", help="outfut file name ")
  args = parser.parse_args()

  name = args.name
  oname = args.oname
  
  if os.path.isfile(name) == True:
    file_lst = open(name,"r")
    if os.path.isfile(oname+".prj") == True:
      os.remove(oname+".prj")
    if os.path.isfile(oname+".ise") == True:
      os.remove(oname+".ise")
    xlx_prj_file = open(oname+".prj","w")
    xlx_ise_file = open(oname+".ise","w")
    timestamp = datetime.datetime.now()  #micro seconds timing
    
    for line in file_lst:
      line = line.strip()
      if line == "" or line[0] == "#":
        pass
      # for .v or .vhd file outputs add a "../" to be able to operate from a "work" subdirectory
      elif line[-2:] == ".v":
        prj_add_verilog(xlx_prj_file, "\"../" + line)
        ise_add_verilog(xlx_ise_file, "\"../" + line)
      elif line[-4:] == ".vhd":
        prj_add_vhdl(xlx_prj_file, "\"../" + line)
        ise_add_vhdl(xlx_ise_file, "\"../" + line)
        
    file_lst.close()

    #xlx_ise_file.write("    <file xil_pn:name=\"../../wr-cores/top/clbv2_ref_design/clbv2_wr_ref_top.ucf\" xil_pn:type=\"FILE_UCF\">\n")
    #xlx_ise_file.write("    <association xil_pn:name=\"Implementation\"/>\n")
    #xlx_ise_file.write("    </file>\n")

    #xlx_ise_file.write("    <property xil_pn:name=\"Generics, Parameters\" xil_pn:value=\"g_dpram_initf=../../../wr-cores/bin/wrpc/wrc_phy16.bram\" xil_pn:valueState=\"non-default\"/>\n")
    #xlx_ise_file.write("    <property xil_pn:name=\"Other Ngdbuild Command Line Options\" xil_pn:value=\"-bm fpga.bmm -sd %lst_Arch1Path%\" xil_pn:valueState=\"non-default\"/>\n")
    #xlx_ise_file.write("    <property xil_pn:name=\"Other Bitgen Command Line Options\" xil_pn:value=\"-g UnconstrainedPins:Allow\" xil_pn:valueState=\"non-default\"/>\n")

    xlx_prj_file.close()
    xlx_ise_file.close()
  else:
    print(name + ": file not found")  

sys.exit()
