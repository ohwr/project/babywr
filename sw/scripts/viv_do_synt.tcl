#-----------------------------------------------------------------------------
# Title      : Vidado Do Synthesis
#-----------------------------------------------------------------------------
# File       : viv_do_synt.tcl
# Author     : Peter Jansweijer <peterj@nikhef.nl>
# Company    : Nikhef
# Created    : 2018-01-24
# Last update: 2021-06-01
# Platform   : FPGA-generics
# Standard   : VHDL
#-----------------------------------------------------------------------------
# Description:
#
# Synthesize the design
#-----------------------------------------------------------------------------
#
# Copyright (c) 2021 Nikhef, Peter Jansweijer
#
# This source file is free software; you can redistribute it   
# and/or modify it under the terms of the GNU Lesser General   
# Public License as published by the Free Software Foundation; 
# either version 2.1 of the License, or (at your option) any   
# later version.                                               
#
# This source is distributed in the hope that it will be       
# useful, but WITHOUT ANY WARRANTY; without even the implied   
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
# PURPOSE.  See the GNU Lesser General Public License for more 
# details.                                                     
#
# You should have received a copy of the GNU Lesser General    
# Public License along with this source; if not, download it   
# from http://www.gnu.org/licenses/lgpl-2.1.html
#
#-----------------------------------------------------------------------------

puts "synthesizing the design"

#set_property strategy Flow_PerfOptimized_high [get_runs synth_1]
#set_property strategy Performance_ExplorePostRoutePhysOpt [get_runs impl_1]

# Insert the command to launch synthesis run here 
launch_runs -verbose synth_1

puts "wait until synthesis done"
# Insert the command to wait on synth_1 run here 
wait_on_run synth_1

