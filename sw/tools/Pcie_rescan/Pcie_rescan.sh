#! /bin/bash
ID=$(lspci | grep "Xilinx Corporation Device 9021" | awk '{print $1}')
echo "removing Pcie entry...."
echo 1 > /sys/bus/pci/devices/0000\:$ID/remove
sleep 1
echo "rescanning Pcie entries...."
echo 1 > /sys/bus/pci/rescan
sleep 1
echo "done"
