/*
 * Copyright (C) 2021 Ton damen, tond@nikhef.nl
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* 
 * File:   sysfs.h
 * Author: Ton Damen, tond@nikhef.nl
 *
 * Created on 15 september 2021, 10:11
 */

#ifndef SYSFS_H
#define SYSFS_H

int populate_sysfs(struct babywr_dev *dev);
void cleanup_sysfs(struct babywr_dev *dev);

#endif /* SYSFS_H */

