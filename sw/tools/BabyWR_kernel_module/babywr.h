/*
 * Copyright (C) 2021 NIKHEF Amsterdam
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* 
 * File:   babywr.h
 * Author: Ton Damen <tond@nikhef.nl>, modified for BabyWR by
 *         Peter Jansweijer <peterj@nikhef.nl>
 *
 * Created on 24 april 2020, 15:19
 * Modified on March 6, 2023, removed Spec7 specific stuff
 * 
 */

#ifndef BABYWR_H
#define BABYWR_H

#include <linux/pci.h>
#include <linux/miscdevice.h>
#include <linux/ioctl.h>

#define BAR_0                   0

#define BAR0_FNAME              "babywr_bar0"

#define PCI_VENDOR_ID_CERN	    0x10dc
#define PCI_VENDOR_ID_XILINX    0x10ee

#define CPU_MEM_SIZE            (196*1024)
#define SYSCON_REG_ADDR         0x100400

#define BABYWR_IOC_MAGIC         0xdc
#define BABYWR_IOCQSIZE          _IO(BABYWR_IOC_MAGIC, 1)


struct babywr_bar {
    int idx, inode_minor;
    void *__iomem base;
    struct miscdevice mdev;
    ulong size, phys;
};

struct babywr_dev {
    struct pci_dev *pdev;
    struct babywr_bar bar0;
    u32 *cpu_mem;
    u32 *syscon_reg_addr;
    
};

#endif /* BABYWR_H */

