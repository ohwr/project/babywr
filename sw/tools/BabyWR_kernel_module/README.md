﻿KERNEL MODULE babywr
--------------------

This kernel module supports the access of the PCI Axi4 memory of the BabyWR card.
Currently the IP of the BabyWR presents one bar: bar0 (1MB).

Building the kernel module:
---------------------------
cd sw/tools/BabyWR_kernel_module/
# make install DEPMOD needs System.map, if needed create symbolic link.
sudo ln -s /boot/System.map-5.19.0-35-generic /usr/src/linux-headers-5.19.0-35-generic/System.map
make clean
make
sudo make install
# If needed, remove earlier kernel module
sudo rmmod /lib/modules/5.19.0-35-generic/extra/babywr.ko
# Insert kernel module (might need disable secure boot in BIOS)
sudo insmod /lib/modules/5.19.0-35-generic/extra/babywr.ko
---------------------------

When the kernel module (babywr.ko) is loaded, the following should be seen among the kernel messages:

babywr:probe_one: PCI device id: [10ee:7022], address: 0000:01:00.0
babywr:probe_one: size bar0 = 1048576, bar4 = 16777216
babywr:babywr_module_init: loaded

The PCI address is here "0000:01:00.0" which gives the following sysfs entries:

- /sys/bus/pci/devices/0000:01:00.0/deadbe
Writing either '0' or '1' will write this values into the SYSCON register of the CPU.

- /sys/bus/pci/devices/0000:01:00.0/cpu_mem
Gives file based access to the CPU memory.

BAR interfaces
--------------

The module creates one file entry in /dev:
/dev/babywr_bar0

These character devices supports access to the memory regions of each bar in a stream-like fashion through systems calls read() and write(). The region boundaries are protected by returning EOF when reading and EFAULT (invalid address) when writing beyond.

Both cp(3) and dd(3) can be used to transfer files to and from the bar memory regions.

The system call ioctl() can be used to query the size of the bar regions:

ioctl(fd, 0xdc01, 0)

returns the size in bytes.

Both devices implements also the mmap(2) system call, which enables the mapping of the BAR regions to user-space memory.

PERMISSIONS
------------

The default permissions for the device files are granted for root only. To customize this, an udev rules file must be created in /usr/lib/udev/rules.d.

This example gives users who are member of group “babywr” read-write access and others only read access:

file 60-babywr.rules contents:
 
KERNEL=="babywr_*", MODE="0664", GROUP="babywr"

To reload the udev config:

# udevadm control --reload

To add the group “babywr”

# addgroup babywr

To add a groups membership to an existing user the following command is given:

# usermod -a -G babywr username

Nb. reload the driver to effectuate the new configuration.


Ton Damen, tond@nilhef.nl
September 2021
Peter Jansweijer, peterj@nikhef.nl
March 2023





