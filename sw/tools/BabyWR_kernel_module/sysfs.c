/*
 * Copyright (C) 2021 NIKHEF Amsterdam
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#define pr_fmt(fmt)     KBUILD_MODNAME ":%s: " fmt, __func__

#include <linux/device.h>
#include "babywr.h"


static ssize_t show_deadbee(struct device *dev, struct device_attribute *attr, char *buf)
{
    struct babywr_dev *babywr = dev_get_drvdata(dev);
    u32 reg = ioread32(babywr->syscon_reg_addr);
    return scnprintf(buf, PAGE_SIZE, "%x", ((reg & 0x10000000) != 0) ? 1 : 0);
}

static ssize_t store_deadbee(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
    unsigned val;
    struct babywr_dev *babywr = dev_get_drvdata(dev);

    if (sscanf(buf, "%u", &val) != 1)
        return -EINVAL;

    if (val == 0)
        iowrite32(0x0deadbee, babywr->syscon_reg_addr);
    else if (val == 1)
        iowrite32(0x1deadbee, babywr->syscon_reg_addr);
    else
        return -EINVAL;

    return count;
}

static DEVICE_ATTR(deadbee, S_IRUSR | S_IWUSR, show_deadbee, store_deadbee);

ssize_t cpu_mem_read(struct file *filep, struct kobject *kobj, struct bin_attribute *attr, char *buf, loff_t pos, size_t size)
{
    struct device *dev = container_of(kobj, struct device, kobj);
    struct babywr_dev *babywr = dev_get_drvdata(dev);
    u32 *src_addr = babywr->cpu_mem + (pos >> 2);
    unsigned n;
    
    pr_debug("pos = %llu, size = %lu\n", pos, size);
    
     /* only 32-bit aligned and 32-bit multiples */
    if (pos & 3)
        return -EPROTO;
    
    for (n = (size >> 2); n > 0; n--) {
        u32 data = ioread32be(src_addr++);  // present the data as big-endian
        memcpy(buf, &data, 4);
        buf += 4;      
    }
   
    return size;
}

ssize_t cpu_mem_write(struct file *filep, struct kobject *kobj, struct bin_attribute *attr, char *buf, loff_t pos, size_t size)
{
    struct device *dev = container_of(kobj, struct device, kobj);
    struct babywr_dev *babywr = dev_get_drvdata(dev);
    u32 *dest_addr = babywr->cpu_mem + (pos >> 2);
    unsigned n;
    
    pr_debug("pos = %llu, size = %lu\n", pos, size);
    
     /* only 32-bit aligned and 32-bit multiples */
    if (pos & 3)
        return -EPROTO;
    
    for (n = (size >> 2); n > 0; n--) {
        u32 data;
        memcpy(&data, buf, 4);
        iowrite32be(data, dest_addr++);   // expect the data to be big-endian
        buf += 4;
    }
   
    return size;
}

static BIN_ATTR(cpu_mem, S_IRUSR | S_IWUSR, cpu_mem_read, cpu_mem_write, CPU_MEM_SIZE);



int populate_sysfs(struct babywr_dev *babywr)
{
    if (device_create_file(&babywr->pdev->dev, &dev_attr_deadbee) < 0 ||
    device_create_bin_file(&babywr->pdev->dev, &bin_attr_cpu_mem) < 0)
        return -1;
    return 0;
}

void cleanup_sysfs(struct babywr_dev *babywr)
{
    device_remove_file(&babywr->pdev->dev, &dev_attr_deadbee);
    device_remove_bin_file(&babywr->pdev->dev, &bin_attr_cpu_mem);
}
