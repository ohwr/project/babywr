/*
 * Copyright (C) 2021 NIKHEF Amsterdam
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* 
 * File:   babywr_mod.c
 * Author: Ton Damen <tond@nikhef.nl>, modified for BabyWR by
 *         Peter Jansweijer <peterj@nikhef.nl>
 *
 * Created on April 23, 2020, 13:14 AM
 * Modified on September 14, 2021, Added BAR4
 * Modified on March 6, 2023, removed Spec7 specific stuff
 */

#define DEBUG


#define pr_fmt(fmt)     KBUILD_MODNAME ":%s: " fmt, __func__

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include "babywr.h"
#include "sysfs.h"
#include "misc_dev.h"


#define MODULE_NAME "babywr"
#define MODULE_DESC "BabyWR experimental Driver"
#define MODULE_RELDATE "September 2021"

MODULE_AUTHOR("<Ton Damen>tond@nikhef.nl");
MODULE_DESCRIPTION(MODULE_DESC);
MODULE_VERSION("0.2");
MODULE_LICENSE("GPL");

//pci_device_id=0x18d pci_vendor_id=0x10dc

static const struct pci_device_id pci_ids[] = {
    { PCI_DEVICE(0x10ee, 0x9021),},
    { 0,}
};

MODULE_DEVICE_TABLE(pci, pci_ids);

static int probe_one(struct pci_dev *pdev, const struct pci_device_id *id) {
    struct babywr_dev *babywr;
    int rc;

    //pr_info("Buid %s %s\n", __DATE__, __TIME__);
    pr_info("PCI device id: [%x:%x], address: %s\n", pdev->vendor, pdev->device, dev_name(&pdev->dev));

    if (pci_enable_device(pdev) < 0)
        return -EINVAL;

    if ((babywr = kzalloc(sizeof (struct babywr_dev), GFP_KERNEL)) == NULL)
        return -ENOMEM;

    babywr->pdev = pdev;
    babywr->bar0.size = pci_resource_len(pdev, BAR_0);
    babywr->bar0.phys = pci_resource_start(pdev, BAR_0);
    babywr->bar0.base = pci_iomap(pdev, BAR_0, babywr->bar0.size);
    babywr->bar0.idx = BAR_0;
    babywr->cpu_mem = babywr->bar0.base;
    babywr->syscon_reg_addr = babywr->bar0.base + SYSCON_REG_ADDR;

    pci_set_drvdata(pdev, babywr);

    populate_sysfs(babywr);

    rc = miscdev_init(BAR0_FNAME, &babywr->bar0.mdev);
    if (rc > 0)
        babywr->bar0.inode_minor = rc;
    else
        return rc;

    pr_info("size bar0 = %lu\n", babywr->bar0.size);

    return 0;
}

static void remove_one(struct pci_dev *pdev) {
    struct babywr_dev *babywr = pci_get_drvdata(pdev);
    misc_deregister(&babywr->bar0.mdev);
    cleanup_sysfs(babywr);
    pci_set_drvdata(pdev, NULL);
    kfree(babywr);
}

static struct pci_driver pci_driver = {
    .name = MODULE_NAME,
    .id_table = pci_ids,
    .probe = probe_one,
    .remove = remove_one
};

static int __init babywr_module_init(void) {
    int rc = pci_register_driver(&pci_driver);
    if (rc == 0)
        pr_info("loaded\n"); // (compiled at %s %s)\n", __DATE__, __TIME__);
    else
        pr_alert("failed. Could not register pci driver.");
    return rc;
}

static void __exit babywr_module_exit(void) {
    pci_unregister_driver(&pci_driver);
    pr_info("removed\n");
}

module_init(babywr_module_init);
module_exit(babywr_module_exit);
