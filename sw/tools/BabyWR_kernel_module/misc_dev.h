/*
 * Copyright (C) 2021 NIKHEF Amsterdam
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* 
 * File:   misc_dev.h
 * Author: Ton Damen <tond@nikhef.nl>
 *
 * Created on April 27, 2020, 7:38 PM
 */

#ifndef MISC_DEV_H
#define MISC_DEV_H

int miscdev_init(const char *, struct miscdevice *mdev);


#endif /* MISC_DEV_H */

