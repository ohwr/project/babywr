BabyWR Tools
-------------------

This is a software collection a various tools related to BabyWR. Currently it holds 4 items

- PCIe_mm, A debug to quickly read and write memory in one of the BARs of the SPEC7.
- PCIe_rescan, A simple script to reload the PCIe entry. Mandatory after each programming.
- PCIe_vuart, A terminal interface to the uart of the WR core (BAR0).
- babywr-kernel-module, Provides /dev/babywr_bar0 entry, and other userspace options. 

PCIe_mm
--------------
This tool can be used to read and write to a certain adress. For example:

    ./babywr-tools/Pcie_mm/mm.py -a 0x0004
Will read address 0x0004.

    ./babywr-tools/Pcie_mm/mm.py -a 0x0004 -w 0xABBA 
Will write 0xABBA to adress 0x0004

PCIe_rescan
------------
This scripts automaticly removes and rescans the BabyWR PCIe device.

    ./babywr-tools/Pcie_rescan/Pcie_rescan.sh
Must be run as root.

PCIe_vuart
------------
A python script to emulate a termios terminal to utilize an uart connection on the White Rabbit ref design.

    ./babywr-tools/Pcie_vuart/vuart.py

babywr-kernel-module
------------
The Kernel module written by the one and only Ton Damen. See that specific readme.md for more info.
It's mandatory to have installed to use all the previous tools (except the SMBus_reset).

Pascal Bos, bosp@nikhef.nl
Ton Damen, tond@nikhef.nl
September 2021




