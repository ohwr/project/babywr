#!/usr/bin/python

"""
Agilent Waveform Generator 33600A remote control

-------------------------------------------------------------------------------
Copyright (C) 2023 Peter Jansweijer, 'stolen' from Tjeerd Pinkert and freely
    adjusted

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
-------------------------------------------------------------------------------

Usage:
  Keysight_33600A.py <IP#>
  Keysight_33600A.py -h | --help

  IP          IP number of the Waveform Generator
              (for example: 192.168.32.242 which is its DevNet IP number)

Options:
  -h --help   Show this screen.
 
"""

import os
import sys
import time
import scipy
import struct
import datetime
import pdb

#TJP: installed from web python-vxi Alex
import vxi11

import matplotlib.pyplot as plt

############################################################################


############################################################################
def wfg_wr_grandmaster(wfg):

  """
  Initialize the Wafeorm Generator for 10 MHz, 1 PPS White Rabbit Grand Master source

  wfg      -- instance of python-vxi connected to the Waveform Generator
  """

  # Configure Channel 1 square 0V, 2V, 10MHz
  # Amplitude: 2V, Offset 1V
  #pdb.set_trace()
  
  wfg.write("SOURce1:FUNCtion SQU")
  wfg.write("SOURce1:FREQuency 10 MHZ")
  wfg.write("SOURce1:VOLTage +2.0")
  wfg.write("SOURce1:VOLTage:OFFSet +1.0")
  wfg.write("SOURce1:VOLTage:LIMit:LOW +0.0")
  wfg.write("SOURce1:VOLTage:LIMit:HIGH +2.0")
  wfg.write("SOURce1:VOLTage:LIMit:STATe ON")
  wfg.write("OUTPut1 ON")

  # Configure Channel 2 square 0V, 2V, 1 Hz
  # Amplitude: 2V, Offset 1V
  wfg.write("SOURce2:FUNCtion PULS")
  wfg.write("SOURce2:PULSe:PERiod 1 s")
  wfg.write("SOURce2:PULSe:WIDTh 100 ms")
  wfg.write("SOURce2:VOLTage +2.0")
  wfg.write("SOURce2:VOLTage:OFFSet +1.0")
  wfg.write("SOURce2:VOLTage:LIMit:LOW +0.0")
  wfg.write("SOURce2:VOLTage:LIMit:HIGH +2.0")
  wfg.write("SOURce2:VOLTage:LIMit:STATe ON")
  wfg.write("OUTPut2 ON")

  return

############################################################################
def wfg_trigger_reset(wfg):

  """
  Initialize the Wafeorm Generator for triggering a reset by driving Channel
  1 from 1.5 volt to 0 volt for 100 ms.
  Note: Calling this function to prepare for a trigger via wfg.write("*TRG")
  already generates a spurrious pulse...

  wfg      -- instance of python-vxi connected to the Waveform Generator
  """

  # Configure Channel 1 square 0V, 1V5, 100 ms low
  # 1 MOhm => Amplitude: 1V5 / 2, Offset 0V75 / 2
  
  wfg.write("SOURce1:FUNCtion PULS")
  wfg.write("SOURce1:PULSe:PERiod 1 S")
  wfg.write("SOURce1:PULSe:WIDTh 100 ms")
  wfg.write("SOURce1:VOLTage +0.75")
  wfg.write("SOURce1:VOLTage:OFFSet +0.375")
  wfg.write("SOURce1:VOLTage:LIMit:LOW +0.0")
  wfg.write("SOURce1:VOLTage:LIMit:HIGH +2.0")
  wfg.write("SOURce1:VOLTage:LIMit:STATe ON")
  wfg.write("TRIGger1:SOURce BUS")
  wfg.write("SOURce1:BURST:NCYCles 1")
  wfg.write("SOURce1:BURST:STATe ON")
  wfg.write("OUTPut1:POLarity INVerted")
  wfg.write("OUTPut1 ON")
  #wfg.write("*TRG")
  #pdb.set_trace()

  # Configure Channel 2 square 0V, 2V, 1 Hz
  # Amplitude: 2V, Offset 1V
  wfg.write("OUTPut2 OFF")

  return

############################################################################
##
## If run from commandline, we can test the library
##
"""
Usage:
  Keysight_33600A.py <IP#>
  Keysight_33600A.py -h | --help

  IP          IP number of the Waveform Generator
              (for example: 192.168.32.234 which is its DevNet IP number)

Options:
  -h --help   Show this screen.

"""
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("wfg", help="IP-Number of the Waveform Generator")
    args = parser.parse_args()
    print ("Use Waveform Generator IP address: " + str(args.wfg))

    wfg =  vxi11.Instrument(args.wfg)    
    #wfg =  vxi11.Instrument("192.168.32.234")
    print(wfg.ask("*IDN?"))
    # Returns 'Agilent Technologies,33622A,MY53800427,A.01.10-2.25-03-64-02'

    #wfg_wr_grandmaster(wfg)
    wfg_trigger_reset(wfg)
    pdb.set_trace()
    wfg.write("*TRG")
    


    sys.exit()
