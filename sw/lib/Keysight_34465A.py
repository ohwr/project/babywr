#!/usr/bin/python

"""
Keysight DMM 34465A remote control

-------------------------------------------------------------------------------
Copyright (C) 2017 Peter Jansweijer, 'stolen' from Tjeerd Pinkert and freely
    adjusted

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
-------------------------------------------------------------------------------

Usage:
  Keysight_34465A.py <IP#>
  Keysight_34465A.py -h | --help

  IP          IP number of the Digital Multimeter
              (for example: 192.168.32.242 which is its DevNet IP number)

Options:
  -h --help   Show this screen.
 
"""

import os
import sys
import time
import scipy
import struct
import datetime

#TJP: installed from web python-vxi Alex
import vxi11

import matplotlib.pyplot as plt

############################################################################


############################################################################
def dmm_init(dmm):

  """
  Initialize the Digital Multimeter for 4-wire resistance (PT-100) measurement.

    dmm      -- instance of python-vxi connected to the Digital Multimeter
  """

  dmm =  vxi11.Instrument(args.dmm)    
  #dmm =  vxi11.Instrument("192.168.32.239")
  print(dmm.ask("*IDN?"))
  # Returns 'Keysight Technologies,34465A,MY57501367,A.02.14-02.40-02.14-00.49-03-01'

  # Configure for 4 wire resistance range 100 ohm
  dmm.write("CONFigure:FRESistance 100")
  dmm.write("SAMPle:COUNt 1")

  return

############################################################################
def pt_100(ohm):

  """
  Calculates the temperature for a given measured PT-100 resistance.

    ohm      -- Ohmic value of the PT-100
  """

  # Rt = R0.(1 + A.t + B.t^2 + C.(t-100).t^3)
  R0 = float(100.0)
  A  = float(3.9083e-3)
  B  = float(-5.7750e-7)
  # C = -4,1430e-12 for t < 0 degrees / 0.0 for t >= 0 degrees
  # For C = 0 =>
  temp = (-R0 * A + (R0**2 * A**2 - 4 * R0 * B * (R0 - ohm))**0.5)/(2 * R0 * B)
  return (temp)

############################################################################
##
## If run from commandline, we can test the library
##
"""
Usage:
  Keysight_34465A.py <IP#>
  Keysight_34465A.py -h | --help

  IP          IP number of the Digital Multimeter
              (for example: 192.168.32.242 which is its DevNet IP number)

Options:
  -h --help   Show this screen.

"""
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("dmm", help="IP-Number of the Digital Multimeter")
    args = parser.parse_args()
    print ("Use Digital Multimeter IP address: " + str(args.dmm))

    dmm =  vxi11.Instrument(args.dmm)    
    dmm_init(dmm)
    # Returns 'Keysight Technologies,34465A,MY57501367,A.02.14-02.40-02.14-00.49-03-01'

    ohm= float(dmm.ask("READ?"))
    print(ohm)
    print(pt_100(ohm))
    print(pt_100(100))
    print(pt_100(138.5))


    sys.exit()
