onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /babywr_blink/clk_125m_dmtd_p_i
add wave -noupdate /babywr_blink/q
add wave -noupdate -expand /babywr_blink/led
add wave -noupdate /babywr_blink/enable
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1114 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {4200 ns}
