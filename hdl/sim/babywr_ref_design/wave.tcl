onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /babywr_ref/gpio1v8(2)
add wave -noupdate /babywr_ref/clk_ref_gth_p_i
add wave -noupdate /babywr_ref/clk_125m_dmtd_p_i
add wave -noupdate /babywr_ref/clk_ext_10m_p_i
add wave -noupdate -divider BUFGMUX
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/clk_dmtd
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/clk_ref_62m5
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/clk_sys_62m5
add wave -noupdate -divider CPU
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/clk_sys_i
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/rst_n_i
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/g_IRAM_SIZE
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/g_IRAM_INIT
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/g_CPU_ID
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/irq_i
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/dwb_i
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/dwb_o
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/host_slave_i
add wave -noupdate /babywr_ref/cmp_xwrc_board_babywr/cmp_board_common/cmp_xwr_core/WRPC/U_CPU/host_slave_o
add wave -noupdate -divider UART
add wave -noupdate /babywr_ref/gpio3v3(0)
add wave -noupdate /babywr_ref/gpio3v3(1)
add wave -noupdate -divider PLL_62m5_500m
add wave -noupdate /babywr_ref/cmp_pll_62m5_500m/areset_n_i
add wave -noupdate /babywr_ref/cmp_pll_62m5_500m/clk_62m5_pllref_i
add wave -noupdate /babywr_ref/cmp_pll_62m5_500m/clk_500m_o
add wave -noupdate /babywr_ref/cmp_pll_62m5_500m/pll_500m_locked_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {245670182 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 564
configure wave -valuecolwidth 210
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {205530543 ps} {315955385 ps}
