# VCom_Functional.tcl
# compile for functional simulation
# From directory ....babywr/hdl/syn/babywr_ref_design
# hdlmake list-files > proj_file_list.txt

vcom -explicit -93 -work work ../../board/wr_sysmone4_wrapper.vhd
vcom -explicit -93 -work work ../../top/babywr_ref_design/pll_62m5_500m.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_dec_8b10b.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_edge_detect.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_enc_8b10b.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_reset.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_reset_multi_aasd.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_serial_dac.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_sync.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_sync_register.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gencores_pkg.vhd
vlog -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_onewire_master/sockit_owm.v
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/simple_uart_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/uart_async_rx.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/uart_async_tx.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/uart_baud_gen.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wishbone_pkg.vhd
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_csr.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_decode.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_divide.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_ecc.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_exceptions.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_fetch.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_multiply.v
vcom -explicit -93 -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_pkg.vhd
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_shifter.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_timer.v
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/xwrf_loopback/lbk_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_crc32_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_registers_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_pps_gen/pps_gen_wb.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_sit5359_interface/sit5359_regs_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_softpll_ng/softpll_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/escape_detector.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/escape_inserter.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/fifo_showahead_adapter.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/wr_streamers_wbgen2_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_tbi_phy/disparity_gen_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_syscon_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/family7-gtx-lp/gtx_comma_detect_lp.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_bit_sync.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gte4_drp_arb.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe4_delay_powergood.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_userclk_rx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_userclk_tx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_userdata_rx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_userdata_tx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_reset_inv_sync.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_reset_sync.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/gtwizard_ultrascale_v1_7_gthe4_channel.v
vcom -explicit -93 -work work ../../board/wr_sysmon.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/axi/axi4_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_crc_gen.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_extend_pulse.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_pulse_synchronizer2.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_sync_ffs.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/genram_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_clock_monitor/clock_monitor_wbgen2_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_crossbar.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_slave_adapter/wb_slave_adapter.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/simple_uart_wb.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_eic.vhd
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_exec.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_regfile.v
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_writeback.v
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/wr_fabric_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/xwrf_loopback/lbk_wishbone_controller.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/timing/dmtd_sampler.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_mdio_regs.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_wishbone_controller.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_mini_nic/minic_wbgen2_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_sit5359_interface/sit5359_if_wb.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_softpll_ng/spll_wbgen2_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/wr_streamers_wb.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_cpu_csr_wbgen2_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_syscon_wb.vhd
vcom -explicit -93 -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/common/lpdc_mdio_regs.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe3_cal_freqcnt.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe4_cal_freqcnt.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_rx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_tx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtwiz_reset.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtye4_cal_freqcnt.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/phy_ref_clk_100/gtwizard_ultrascale_2_gthe4_channel_wrapper.v
vcom -explicit -93 -work work ../../top/babywr_ref_design/gen_x_mhz.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_async_counter_diff.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/common/gc_pulse_synchronizer.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/common/inferred_async_fifo.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/common/inferred_async_fifo_dual_rst.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/memory_loader_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/xilinx/gc_shiftreg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_axi4lite_bridge/xwb_axi4lite_bridge.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_clock_monitor/clock_monitor_wb.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_crossbar/sdb_rom.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_gpio_port/wb_gpio_port.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_onewire_master/wb_onewire_master.vhd
vlog -work work ../../wr-cores/ip_cores/urv-core/rtl/urv_cpu.v
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/xwrf_mux.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/timing/dmtd_with_deglitcher.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/timing/pulse_stamper_sync.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/endpoint_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_mini_nic/minic_wb_slave.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_pps_gen/wr_pps_gen.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_sit5359_interface/wr_sit5359_interface.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_softpll_ng/spll_aligner.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/dropping_buffer.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/fixed_latency_ts_match.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/ts_restore_tai.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_cpu_csr_wb.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe3_cpll_cal.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_rx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_tx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtye4_cpll_cal_rx.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtye4_cpll_cal_tx.v
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/common/generic_shiftreg_fifo.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/generic/generic_async_fifo.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/generic/generic_async_fifo_dual_rst.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_dualclock.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_sameclock.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/xilinx/generic_dpram_split.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_clock_monitor/xwb_clock_monitor.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_crossbar/xwb_sdb_crossbar.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_gpio_port/xwb_gpio_port.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_onewire_master/xwb_onewire_master.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/timing/dmtd_phase_meas.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/timing/pulse_stamper.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/endpoint_private_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_pps_gen/xwr_pps_gen.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_sit5359_interface/xwr_sit5359_interface.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrcore_pkg.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gthe4_cpll_cal.v
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/common/gtwizard_ultrascale_v1_7_gtye4_cpll_cal.v
vcom -explicit -93 -work work ../../wr-cores/platform/xilinx/wr_xilinx_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/xilinx/generic_dpram.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/xwb_fabric_sink.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/xwb_fabric_source.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_autonegotiation.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_clock_alignment_fifo.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_leds_controller.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rtu_header_extract.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_crc_size_check.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_early_address_match.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_oob_insert.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_status_reg_insert.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_vlan_unit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_wb_master.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_sync_detect.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_sync_detect_16bit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_ts_counter.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_crc_inserter.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_header_processor.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_inject_ctrl.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_packet_injection.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_pcs_16bit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_pcs_8bit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/streamers_pkg.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/gtwizard_ultrascale_2_gtwizard_gthe4.v
vcom -explicit -93 -work work ../../wr-cores/board/common/wr_board_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/common/inferred_sync_fifo.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_packet_filter.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_pcs_16bit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_pcs_8bit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_timestamping_unit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_vlan_unit.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/streamers_priv_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/xrx_streamers_stats.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/xtx_streamers_stats.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_diags_dpram.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_urv_wrapper.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/gtwizard_ultrascale_2_gtwizard_top.v
vcom -explicit -93 -work work ../../board/wr_babywr_pkg.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/genrams/generic/generic_sync_fifo.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_1000basex_pcs.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_tx_path.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/fixed_latency_delay.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/xrtx_streamers_stats.vhd
vlog -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/phy_ref_clk_100/gtwizard_ultrascale_2.v
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/wb_simple_uart.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wbgen2/wbgen2_fifo_sync.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/fabric/xwrf_loopback/xwrf_loopback.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_buffer.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_mini_nic/wr_mini_nic.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/xrx_streamer.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/xtx_streamer.vhd
vcom -explicit -93 -work work ../../wr-cores/platform/xilinx/wr_gtp_phy/family7-gthe4-lp/wr_gthe4_phy_family7_lp.vhd
vcom -explicit -93 -work work ../../wr-cores/ip_cores/general-cores/modules/wishbone/wb_uart/xwb_simple_uart.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/ep_rx_path.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_mini_nic/xwr_mini_nic.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_softpll_ng/spll_wb_slave.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_streamers/xwr_streamers.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/wr_endpoint.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_softpll_ng/wr_softpll_ng.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wrc_periph.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_endpoint/xwr_endpoint.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wr_softpll_ng/xwr_softpll_ng.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/wr_core.vhd
vcom -explicit -93 -work work ../../wr-cores/modules/wrc_core/xwr_core.vhd
vcom -explicit -93 -work work ../../wr-cores/board/common/xwrc_board_common.vhd
vcom -explicit -93 -work work ../../board/xwrc_board_babywr.vhd
vcom -explicit -93 -work work ../../top/babywr_ref_design/babywr_ref.vhd


# Executing Vivado Block Design script:
#../../ip/pcie_1x.bd
#../../top/babywr_ref_design/babywr_ref.xdc
#../../top/babywr_ref_design/babywr_ref.bmm
#../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/gtwizard_ultrascale_2.xdc
#../../wr-cores/platform/xilinx/wr_gtp_phy/xilinx-ip/gthe4_lp/phy_ref_clk_100/gtwizard_ultrascale_2_ooc.xdc

# include hardware version ID un FPGA USER_ID register:
#hdl_version.xdc
