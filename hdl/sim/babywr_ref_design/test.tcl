# reset_n_i = gpio1v8(2)
force /babywr_ref/gpio1v8(2) 0,1 200 ns

# uart_rxd_i = gpio3v3(0)
force /babywr_ref/gpio3v3(0) 0

force /babywr_ref/clk_ref_gth_p_i 0, 1 5000 ps -rep 10000 ps
force /babywr_ref/clk_ref_gth_n_i 1, 0 5000 ps -rep 10000 ps

force /babywr_ref/clk_125m_dmtd_p_i 0, 1 4001 ps -rep 8002 ps
force /babywr_ref/clk_125m_dmtd_n_i 1, 0 4001 ps -rep 8002 ps

# receive <K28.5><D16.2>
force /babywr_ref/sfp_rxp_i 0, 1 1600 ps, 0 5600 ps, 1 6400 ps, 0 7200 ps, 1 8000 ps, 0 8800 ps, 1 10400 ps, 0 11200 ps, 1 13600 ps, 0 14400 ps, 1 15200 ps -rep 16 ns
force /babywr_ref/sfp_rxn_i 1, 0 1600 ps, 1 5600 ps, 0 6400 ps, 1 7200 ps, 0 8000 ps, 1 8800 ps, 0 10400 ps, 1 11200 ps, 0 13600 ps, 1 14400 ps, 0 15200 ps -rep 16 ns

# It takes a while before the RiscV is initiated and uart_txd_o (= gpio3v3(1))
# spids out "WR Core: starting up..."
run 27 ms
