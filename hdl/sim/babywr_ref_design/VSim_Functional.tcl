set Simulation TRUE

if {$Simulation} {
    puts "Note: Simulation"
	  set g_simulation 1
	} else {
    puts "Note: Synthesis"
	  set g_simulation 0
	}

source ../../../sw/scripts/VSim_Current_Revision.tcl

puts "elf file used for CPU in WRPC: [set elf_file_wrpc_cpu "..\\..\\..\\sw\\precompiled\\wrps-sw-v5_babywr\\wrc.elf"]"
set wrpc_cpu_instpath "wrpc_cpu_memory"

# !!! Note !!!: Don't forget to compile the software (elf file) for simulation (avoid printf etc. to speed up simulation time)
# !!! Note !!!: The double \\ are there since the DOS command below needs backslashes and a single backslash is seen as a switch in tcl

# Generate a "wrpc_cpu_memory.mem" file from the "elf" file content
exec cmd.exe /c updatemem -meminfo babywr_ref_top.smi -data $elf_file_wrpc_cpu -proc $wrpc_cpu_instpath -force

# Convert the "mem" to a "bram" (a format used by the White Rabbit "memory_loader_pkg.vhd")
do ../../../sw/scripts/mem2bram.tcl wrpc_cpu_memory 196608
# Now a fresh "wrpc_cpu_memory.bram" is in place for simulation and is loaded into xwb_dpram

# Note that -novopt causes No Optimization (some internal signals might get non-vivible by optimization)
# Note that "-L unisim" is needed to find the primitive "BSCANE2" thta is instantiated in "$LM32_Sources/platform/kintex7/jtag_tap.v "
#suppress warning Warning: (vsim-151) NUMERIC_STD.TO_INTEGER: Value <> is not in bounds of NATURAL.
#suppress warning Warning: (vsim-8684) No drivers exist on out port <blabla>

# -novopt is now deprecated
#vsim -voptargs="+acc" -novopt 
vsim -voptargs="+acc=lnprv" \
  -G/babywr_ref/g_simulation=$g_simulation \
  -G/babywr_ref/g_dpram_initf=wrpc_cpu_memory.bram \
  -G/babywr_ref/g_ref_clk_freq="125MHz" \
  -t ps -L unisim -lib work work.babywr_ref

# Depending on what needs to be simultated
do wave.tcl
do test.tcl

view signals

#run 100 us

#stop

wave zoom full

#
# End
#
