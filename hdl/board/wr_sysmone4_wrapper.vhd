-------------------------------------------------------------------------------
-- Title      : Xilinx sysmone4 wrapper
-- Project    : BabyWR
-- URL        : https://docs.amd.com/r/en-US/ug974-vivado-ultrascale-libraries/SYSMONE4
-------------------------------------------------------------------------------
-- File       : wr_sysmone4_wrapper.vhd
-- Author(s)  : Peter Jansweijer <peterj@nikhef.nl>
-- Company    : Nikhef
-- Created    : 2024-07-10
-- Last update: 2024-07-10
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: A platform (Ultrascale+) dependent instantiation of SYSMONE4.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2024 Nikhef
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author    Description
-- 2024-07-10  0.1      PeterJ    Initial release
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity wr_sysmone4_wrapper is
  port (
    drp_clk_i         : in  std_logic;
    drp_rst_i         : in  std_logic;
    drp_addr_i        : in  std_logic_vector(7 downto 0);
    drp_din_i         : in  std_logic_vector(15 downto 0);
    drp_dout_o        : out std_logic_vector(15 downto 0);
    drp_we_i          : in  std_logic;
    drp_rdy_o         : out std_logic;
    drp_en_i          : in  std_logic
  );
end wr_sysmone4_wrapper;

architecture rtl of wr_sysmone4_wrapper is

begin

-- SYSMONE4: AMD Analog-to-Digital Converter and System Monitor
--           UltraScale
-- Xilinx HDL Language Template, version 2024.1

SYSMONE4_inst : SYSMONE4
generic map (
   -- INIT_40 - INIT_44: SYSMON configuration registers
   INIT_40 => X"0000",
   INIT_41 => X"0000",
   INIT_42 => X"0000",
   INIT_43 => X"0000",
   INIT_44 => X"0000",
   INIT_45 => X"0000",               -- Analog Bus Register.
   -- INIT_46 - INIT_4F: Sequence Registers
   INIT_46 => X"0000",
   INIT_47 => X"0000",
   INIT_48 => X"0000",
   INIT_49 => X"0000",
   INIT_4A => X"0000",
   INIT_4B => X"0000",
   INIT_4C => X"0000",
   INIT_4D => X"0000",
   INIT_4E => X"0000",
   INIT_4F => X"0000",
   -- INIT_50 - INIT_5F: Alarm Limit Registers
   INIT_50 => X"0000",
   INIT_51 => X"0000",
   INIT_52 => X"0000",
   INIT_53 => X"0000",
   INIT_54 => X"0000",
   INIT_55 => X"0000",
   INIT_56 => X"0000",
   INIT_57 => X"0000",
   INIT_58 => X"0000",
   INIT_59 => X"0000",
   INIT_5A => X"0000",
   INIT_5B => X"0000",
   INIT_5C => X"0000",
   INIT_5D => X"0000",
   INIT_5E => X"0000",
   INIT_5F => X"0000",
   -- INIT_60 - INIT_6F: User Supply Alarms
   INIT_60 => X"0000",
   INIT_61 => X"0000",
   INIT_62 => X"0000",
   INIT_63 => X"0000",
   INIT_64 => X"0000",
   INIT_65 => X"0000",
   INIT_66 => X"0000",
   INIT_67 => X"0000",
   INIT_68 => X"0000",
   INIT_69 => X"0000",
   INIT_6A => X"0000",
   INIT_6B => X"0000",
   INIT_6C => X"0000",
   INIT_6D => X"0000",
   INIT_6E => X"0000",
   INIT_6F => X"0000",
   -- Primitive attributes: Primitive Attributes
   COMMON_N_SOURCE => X"ffff",       -- Sets the auxiliary analog input that is used for the Common-N input.
   -- Programmable Inversion Attributes: Specifies the use of the built-in programmable inversion on
   -- specific pins
   IS_CONVSTCLK_INVERTED => '0',     -- Optional inversion for CONVSTCLK, 0-1
   IS_DCLK_INVERTED => '0',          -- Optional inversion for DCLK, 0-1
   -- Simulation attributes: Set for proper simulation behavior
   SIM_DEVICE => "ULTRASCALE_PLUS",  -- Sets the correct target device for simulation functionality.
   SIM_MONITOR_FILE => "design.txt", -- Analog simulation data file name
   -- User Voltage Monitor: SYSMON User voltage monitor
   SYSMON_VUSER0_BANK => 0,          -- Specify IO Bank for User0
   SYSMON_VUSER0_MONITOR => "NONE",  -- Specify Voltage for User0
   SYSMON_VUSER1_BANK => 0,          -- Specify IO Bank for User1
   SYSMON_VUSER1_MONITOR => "NONE",  -- Specify Voltage for User1
   SYSMON_VUSER2_BANK => 0,          -- Specify IO Bank for User2
   SYSMON_VUSER2_MONITOR => "NONE",  -- Specify Voltage for User2
   SYSMON_VUSER3_MONITOR => "NONE"   -- Specify Voltage for User3
)
port map (
   -- ALARMS outputs: ALM, OT
   ALM          => open,              -- 16-bit output: Output alarm for temp, Vccint, Vccaux and Vccbram
   OT           => open,              -- 1-bit output: Over-Temperature alarm
   -- Direct Data Out outputs: ADC_DATA
   ADC_DATA     => open,              -- 16-bit output: Direct Data Out
   -- Dynamic Reconfiguration Port (DRP) outputs: Dynamic Reconfiguration Ports
   DO           => drp_dout_o,        -- 16-bit output: DRP output data bus
   DRDY         => drp_rdy_o,         -- 1-bit output: DRP data ready
   -- I2C Interface outputs: Ports used with the I2C DRP interface
   I2C_SCLK_TS  => open,              -- 1-bit output: I2C_SCLK output port
   I2C_SDA_TS   => open,              -- 1-bit output: I2C_SDA_TS output port
   SMBALERT_TS  => open,              -- 1-bit output: Output control signal for SMBALERT.
   -- STATUS outputs: SYSMON status ports
   BUSY         => open,              -- 1-bit output: System Monitor busy output
   CHANNEL      => open,              -- 6-bit output: Channel selection outputs
   EOC          => open,              -- 1-bit output: End of Conversion
   EOS          => open,              -- 1-bit output: End of Sequence
   JTAGBUSY     => open,              -- 1-bit output: JTAG DRP transaction in progress output
   JTAGLOCKED   => open,              -- 1-bit output: JTAG requested DRP port lock
   JTAGMODIFIED => open,              -- 1-bit output: JTAG Write to the DRP has occurred
   MUXADDR      => open,              -- 5-bit output: External MUX channel decode
   -- Auxiliary Analog-Input Pairs inputs: VAUXP[15:0], VAUXN[15:0]
   VAUXN        => (others => '0'),   -- 16-bit input: N-side auxiliary analog input
   VAUXP        => (others => '1'),   -- 16-bit input: P-side auxiliary analog input
   -- CONTROL and CLOCK inputs: Reset, conversion start and clock inputs
   CONVST       => '0',               -- 1-bit input: Convert start input
   CONVSTCLK    => '0',               -- 1-bit input: Convert start input
   RESET        => drp_rst_i,         -- 1-bit input: Active-High reset
   -- Dedicated Analog Input Pair inputs: VP/VN
   VN           => '0',               -- 1-bit input: N-side analog input
   VP           => '1',               -- 1-bit input: P-side analog input
   -- Dynamic Reconfiguration Port (DRP) inputs: Dynamic Reconfiguration Ports
   DADDR        => drp_addr_i,        -- 8-bit input: DRP address bus
   DCLK         => drp_clk_i,         -- 1-bit input: DRP clock
   DEN          => drp_en_i,          -- 1-bit input: DRP enable signal
   DI           => drp_din_i,         -- 16-bit input: DRP input data bus
   DWE          => drp_we_i,          -- 1-bit input: DRP write enable
   -- I2C Interface inputs: Ports used with the I2C DRP interface
   I2C_SCLK     => '0',               -- 1-bit input: I2C_SCLK input port
   I2C_SDA      => '1'                -- 1-bit input: I2C_SDA input port
);

-- End of SYSMONE4_inst instantiation

end rtl;
