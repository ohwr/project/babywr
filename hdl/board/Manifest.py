files = [
    "wr_babywr_pkg.vhd",
    "wr_sysmon.vhd",
    "wr_sysmone4_wrapper.vhd",
    "wrc_board_babywr.vhd",
    "xwrc_board_babywr.vhd",
]

modules = {
    "local" : [
        "../wr-cores/board/common",
    ]
}
