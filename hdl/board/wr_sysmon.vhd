-------------------------------------------------------------------------------
-- Title      : Xilinx sysmon connect to wishbone
-- Project    : BabyWR
-------------------------------------------------------------------------------
-- File       : wr_sysmon.vhd
-- Author(s)  : Peter Jansweijer <peterj@nikhef.nl>
-- Company    : Nikhef
-- Created    : 2024-07-10
-- Last update: 2024-07-10
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: A module that translates wishbone bus into the Dynaminc
-- Reconfiguration Port (DRP) of a platform dependent sysmon/xadc instantiation.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2024 Nikhef
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author    Description
-- 2024-07-10  0.1      PeterJ    Initial release
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.wishbone_pkg.all;

entity wr_sysmon is
  port (
    clk_sys_i       : in std_logic;
    rst_sys_n_i     : in std_logic;
    slave_i         : in  t_wishbone_slave_in;
    slave_o         : out t_wishbone_slave_out
  );
end wr_sysmon;

architecture rtl of wr_sysmon is
  ---------------------------------------------------------------------------
  -- wishbone signals
  ---------------------------------------------------------------------------
  signal drp_regs_in            : t_wishbone_slave_out;
  signal drp_regs_out           : t_wishbone_slave_in;

  ---------------------------------------------------------------------------
  -- DRP signals
  ---------------------------------------------------------------------------
  signal drp_rst                : std_logic;
  signal drp_addr               : std_logic_vector(7 downto 0);
  signal drp_din                : std_logic_vector(15 downto 0);
  signal drp_dout               : std_logic_vector(15 downto 0);
  signal drp_we                 : std_logic;
  signal drp_rdy                : std_logic;
  signal drp_en                 : std_logic;
  signal drp_in_progress        : std_logic;

  component wr_sysmone4_wrapper
    port (
      drp_clk_i         : in  std_logic;
      drp_rst_i         : in  std_logic;
      drp_addr_i        : in  std_logic_vector(7 downto 0);
      drp_din_i         : in  std_logic_vector(15 downto 0);
      drp_dout_o        : out std_logic_vector(15 downto 0);
      drp_we_i          : in  std_logic;
      drp_rdy_o         : out std_logic;
      drp_en_i          : in  std_logic
    );
  end component wr_sysmone4_wrapper;

begin

  slave_o      <= drp_regs_in;
  drp_regs_out <= slave_i;
  
  drp_rst <= not rst_sys_n_i;
  
  p_translate_wb_xdrp : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if (rst_sys_n_i = '0') then
        drp_en              <= '0';
        drp_we              <= '0';
        drp_regs_in.ack     <= '0';
        drp_regs_in.stall   <= '0';
        drp_regs_in.err     <= '0';
        drp_regs_in.rty     <= '0';
        drp_in_progress     <= '0';
      else
        if drp_in_progress = '0' then
          if(drp_regs_out.cyc = '1' and drp_regs_out.stb = '1' and drp_regs_in.ack = '0') then
            drp_en              <= '1';
            drp_we              <= drp_regs_out.we;
            drp_addr            <= drp_regs_out.adr(9 downto 2);
            drp_din             <= drp_regs_out.dat(15 downto 0);
            drp_in_progress     <= '1';
            drp_regs_in.stall   <= '1';
          else
            drp_regs_in.ack     <= '0';
            drp_regs_in.stall   <= '0';
          end if;
        else
          drp_en <= '0';
          drp_we <= '0';

          if drp_rdy = '1' then
            drp_in_progress              <= '0';
            drp_regs_in.dat(15 downto 0) <= drp_dout;
            drp_regs_in.ack              <= '1';
          end if;
        end if;
      end if;
    end if;
  end process;

  U_sysmon: wr_sysmone4_wrapper
    port map (
      drp_clk_i  => clk_sys_i,
      drp_rst_i  => drp_rst,
      drp_addr_i => drp_addr,
	  drp_din_i  => drp_din,
      drp_dout_o => drp_dout,
      drp_we_i   => drp_we,
      drp_rdy_o  => drp_rdy,
      drp_en_i   => drp_en
    );

end rtl;
