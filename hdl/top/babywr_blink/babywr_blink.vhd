-------------------------------------------------------------------------------
-- Title      : Simple LED blinkdesign for BabyWR
-- Project    : BabyWR 
-- URL        : https://ohwr.org/project/babywr/wikis/home
-------------------------------------------------------------------------------
-- File       : babywr_blink.vhd
-- Author(s)  : Peter Jansweijer <peterj@nikhef.nl>
-- Company    : Nikhef
-- Created    : 2022-11-30
-- Last update: 2022-11-30
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: Top-level file for simple BabyWR LED blinkd design
-------------------------------------------------------------------------------
-- Copyright (c) 2022 Nikhef
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee, unisim;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use unisim.VCOMPONENTS.all;

entity babywr_blink is
  generic(
    g_q : natural := 24);
  port (
    clk_125m_dmtd_n_i : in     std_logic;
    clk_125m_dmtd_p_i : in     std_logic;
    led               : out    std_logic_vector(7 downto 0)
    );
end entity babywr_blink;

architecture structure of babywr_blink is

  signal clk_125m_dmtd : std_logic;
  signal q             : std_logic_vector(31 downto 0);
  signal q_del         : std_logic;
  signal enable        : std_logic;

begin
  cmp_ibufds_clk_125m_dmtd : IBUFDS
    generic map(
      CAPACITANCE      => "DONT_CARE",
      DIFF_TERM        => TRUE,
      IBUF_DELAY_VALUE => "0",
      IBUF_LOW_PWR     => TRUE,
      IFD_DELAY_VALUE  => "AUTO",
      IOSTANDARD       => "DEFAULT",
      DQS_BIAS         => "FALSE")
    port map(
      O  => clk_125m_dmtd,
      I  => clk_125m_dmtd_p_i,
      IB => clk_125m_dmtd_n_i);

   process (clk_125m_dmtd)
      variable Count: Unsigned(31 downto 0) := (others => '0');
   begin       
      if rising_edge(clk_125m_dmtd) then
         count := count + 1;
      end if;
      q <= Std_Logic_Vector (count);
   end process;

  --For normal operation choose Q24 (@ 200 MHz)
  --For quick simulation choose Q4

   process (clk_125m_dmtd)
   begin       
      if rising_edge(clk_125m_dmtd) then
         q_del <= q(g_q);
      end if;
      enable <= q(g_q) and not q_del;
   end process;

   process (clk_125m_dmtd)
      variable cnt : unsigned(3 downto 0) := (others => '0');
   begin       
      if rising_edge(clk_125m_dmtd) then
         if enable = '1' then
            if (cnt = x"5") then
               cnt := (others => '0');
            else
               cnt := cnt + 1;
            end if;
         end if;
         -- LEDs on BabyWR-Carier led[7:4]
         -- LEDs on BabyWR led[3:0]
         case cnt is
            when x"0" => led <= "00010001";
            when x"1" => led <= "00100010";
            when x"2" => led <= "01000100";
            when x"3" => led <= "10001000";
            when x"4" => led <= "01000100";
            when x"5" => led <= "00100010";
            when others => led <= "01010101";
         end case;
      end if;
   end process;

end architecture structure ; -- of babywr_blink

