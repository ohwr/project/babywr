fetchto = "../../wr-cores"
fetchto = "../../wr-cores/ip_cores"

files = [
    "gen_x_mhz.vhd",
    "pll_62m5_500m.vhd",
    "wr_irigb_conv.vhd",
    "babywr_ref.vhd",
    "babywr_ext_2260-D6-M.xdc",
    "babywr_ref_2260-D6-M.xdc",
    "babywr_ref_2280-D6-M.xdc",
    "babywr_ref.bmm",
]

modules = {
    "local" : [
        "../../board/",
        "../../wr-cores/",
    ],
    "git" : [
        "git://ohwr.org/project/wr-cores.git",
    ],
    "git" : [
        "git://ohwr.org/hdl-core-lib/general-cores.git",
        "git://ohwr.org/project/urv-core.git",
    ], 
}
