# Packages:
# SBVB484: Bank: 64, 65, 66, 84, 85, 224, 225, 226
# UBVA386: Bank: 65, 66, 84, 224, 225

#   ---------------------------------------------------------------------------`
#   -- Clocks/resets
#   ---------------------------------------------------------------------------
#   -- External oscillator
# Bank 224 -- 125.000 MHz GTH reference MGTREFCLK1
set_property PACKAGE_PIN p6 [get_ports clk_ref_gth_p_i]
set_property PACKAGE_PIN p5 [get_ports clk_ref_gth_n_i]

#   -- Local oscillators
# Bank 224 -- 125.000 MHz GTH reference MGTREFCLK0
#set_property PACKAGE_PIN V6 [get_ports clk_ref_gth_p_i]
#set_property PACKAGE_PIN V5 [get_ports clk_ref_gth_n_i]

# Bank 224 -- 100.000 / 125.000 MHz GTH reference MGTREFCLK1
#set_property PACKAGE_PIN P6 [get_ports ext_ref_p_i]
#set_property PACKAGE_PIN P5 [get_ports ext_ref_n_i]

# Bank 225 -- ???.000 MHz GTH reference MGTREFCLK1
#set_property PACKAGE_PIN K6 [get_ports wfl_clk0_p_o]
#set_property PACKAGE_PIN K5 [get_ports wfl_clk0_n_o]

# Bank 64 (HP) VCCO - 1.8 V -- 124.992 MHz DMTD clock
set_property PACKAGE_PIN V19 [get_ports clk_125m_dmtd_p_i]
set_property IOSTANDARD LVDS [get_ports clk_125m_dmtd_p_i]
set_property PACKAGE_PIN W19 [get_ports clk_125m_dmtd_n_i]
set_property IOSTANDARD LVDS [get_ports clk_125m_dmtd_n_i]

create_clock -period 10.000 -name clk_ref_gth -waveform {0.000 5.000} [get_ports clk_ref_gth_p_i]
create_clock -period 8.000  -name clk_125m_dmtd -waveform {0.000 4.000} [get_ports clk_125m_dmtd_p_i]

# Set divide by 2 property for generated clk_dmtd (former platform xilinx: g_direct_dmtd = TRUE)
create_generated_clock -name clk_125m_dmtd_div2 -source [get_ports clk_125m_dmtd_p_i] -divide_by 2 [get_pins cmp_xwrc_board_babywr/clk_dmtd_reg/Q]

# clocks GTH TX/RX clocks and clk_ref_gth_odiv2 are auto derived. Rename GTH TX/RX clocks to TXOUTCLK and RXOUTCLK
create_generated_clock -name RXOUTCLK      [get_pins cmp_xwrc_board_babywr/cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_2_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[0].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST/RXOUTCLK]
create_generated_clock -name TXOUTCLK      [get_pins cmp_xwrc_board_babywr/cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_2_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[0].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST/TXOUTCLK]

# Bank 66 (HP) VCCO - 1.8 V -- 10 MHz external clock
set_property PACKAGE_PIN C19 [get_ports clk_ext_10m_p_i]
set_property IOSTANDARD LVDS [get_ports clk_ext_10m_p_i]
set_property PACKAGE_PIN B19 [get_ports clk_ext_10m_n_i]
set_property IOSTANDARD LVDS [get_ports clk_ext_10m_n_i]
create_clock -period 100.000 -name clk_ext_10m -waveform {0.000 50.000} [get_ports clk_ext_10m_p_i]

# Using "[get_clocks "clk_ref_62m5*"]" finds either "clk_ref_62m5_pll" or "clk_ref_62m5" depending on 100 or 125 MHz desing 
set_clock_groups -asynchronous \
-group clk_ref_gth \
-group [get_clocks "clk_ref_62m5*"] \
-group clk_125m_dmtd_div2 \
-group RXOUTCLK \
-group TXOUTCLK \
-group clk_ext_10m

set_clock_groups -name ext_mul -asynchronous \
-group [get_clocks -of_objects [get_pins cmp_xwrc_board_babywr/gen_ext_ref_pll.mmcm_adv_inst/CLKOUT0]] \
-group [get_clocks clk_125m_dmtd_div2]

# clk_ref_62m5_div2 = 16 ns = 8 clock periods of clk_500m which has 2 ns period
# Setup requirement at edge 8, hold requirement at edge 7
# See also:
# https://www.xilinx.com/video/hardware/timing-exception-multicycle-path-constraints.html
# See: "Multicycle Path and Positive phase shift" (due to the clk_ref_62m5_div2 to clk_500 delay through MMCME2_ADV)
# Note:
# If clk_ref_gth is sourced from BUFG_GT then use [get_clocks clk_ref_gth] <=> [get_clocks  "*clk_500m*"]
# else if sourced from TXOUTCLK then use [get_clocks TXOUTCLK] <=> [get_clocks  "*clk_500m*"].
#set_multicycle_path 2 -setup -from [get_clocks TXOUTCLK] -to [get_clocks  "*clk_500m*"]
#set_multicycle_path 1 -hold -from [get_clocks TXOUTCLK] -to [get_clocks  "*clk_500m*"]
#set_multicycle_path 3 -setup -start -from [get_clocks  "*clk_500m*"] -to [get_clocks TXOUTCLK]
#set_multicycle_path 2 -hold -start -from [get_clocks  "*clk_500m*"] -to [get_clocks TXOUTCLK]
set_multicycle_path 2 -setup -from [get_clocks clk_ref_gth] -to [get_clocks  "*clk_500m*"]
set_multicycle_path 1 -hold -from [get_clocks clk_ref_gth] -to [get_clocks  "*clk_500m*"]
set_multicycle_path 3 -setup -start -from [get_clocks  "*clk_500m*"] -to [get_clocks clk_ref_gth]
set_multicycle_path 2 -hold -start -from [get_clocks  "*clk_500m*"] -to [get_clocks clk_ref_gth]

#   ---------------------------------------------------------------------------
#   -- PCIe 
#   ---------------------------------------------------------------------------
# Bank 225 (GTH)
set_property PACKAGE_PIN P1  [get_ports pcie_rxn]
set_property PACKAGE_PIN P2  [get_ports pcie_rxp]
set_property PACKAGE_PIN N3  [get_ports pcie_txn]
set_property PACKAGE_PIN N4  [get_ports pcie_txp]

# Bank 225 -- 100.000 MHz GTH reference MGTREFCLK0
set_property PACKAGE_PIN M6 [get_ports pci_clk_p]
set_property PACKAGE_PIN M5 [get_ports pci_clk_n]
create_clock -period 10.000 -name pci_clk_p [get_ports pci_clk_p]

# Bank 84 (HD) VCCO - 3.3 V
set_property PACKAGE_PIN U13 [get_ports perst_n]
set_property IOSTANDARD LVCMOS33 [get_ports perst_n]

#   ---------------------------------------------------------------------------
#   -- I2C SiTime5359 Reference & DMTD Oscillators
#   ---------------------------------------------------------------------------

# Bank 85 (HD) VCCO - 3.3 V
set_property PACKAGE_PIN A12 [get_ports refclk_sda]
set_property IOSTANDARD LVCMOS33 [get_ports refclk_sda]
set_property PACKAGE_PIN A10 [get_ports refclk_scl]
set_property IOSTANDARD LVCMOS33 [get_ports refclk_scl]
# Bank 84 (HD) VCCO - 3.3 V
set_property PACKAGE_PIN T12 [get_ports dmtd_sda]
set_property IOSTANDARD LVCMOS33 [get_ports dmtd_sda]
set_property PACKAGE_PIN V12 [get_ports dmtd_scl]
set_property IOSTANDARD LVCMOS33 [get_ports dmtd_scl]

#   ---------------------------------------------------------------------------
#   -- SFP I/O for transceiver
#   ---------------------------------------------------------------------------

# Bank 224 (GTH)
set_property PACKAGE_PIN AA3 [get_ports sfp_rxn_i]
set_property PACKAGE_PIN AA4 [get_ports sfp_rxp_i]
set_property PACKAGE_PIN Y5  [get_ports sfp_txn_o]
set_property PACKAGE_PIN Y6  [get_ports sfp_txp_o]

# Bank 64 (HD) VCCO - 1.8 V
# sfp detect
set_property PACKAGE_PIN T17 [get_ports sfp_mod_def0_i]
set_property IOSTANDARD LVCMOS18 [get_ports sfp_mod_def0_i]
# scl
set_property PACKAGE_PIN U19 [get_ports sfp_mod_def1_b]
set_property IOSTANDARD LVCMOS18 [get_ports sfp_mod_def1_b]
# sda
set_property PACKAGE_PIN U18 [get_ports sfp_mod_def2_b]
set_property IOSTANDARD LVCMOS18 [get_ports sfp_mod_def2_b]

# Bank 84 (HD) VCCO - 3.3 V
set_property PACKAGE_PIN Y10 [get_ports sfp_los_fault_i]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_los_fault_i]
set_property PACKAGE_PIN Y11 [get_ports sfp_tx_disable_o]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_tx_disable_o]

#   ---------------------------------------------------------------------------
#   -- UART
#   ---------------------------------------------------------------------------

# Signal uart_txd_o is an output in the design and must be connected to pin 20/12 (RXD_I) of CP2105GM
# Signal uart_rxd_i is an input in the design and must be connected to pin 21/13 (TXD_O) of CP2105GM
# Rx signals are pulled down so the USB on the CLB and the USB on the G-Board can be OR-ed

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 36). Used for uart_rxd_i
set_property PACKAGE_PIN AA11 [get_ports {gpio3v3[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[0]}]

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 34). Used for uart_txd_o
set_property PACKAGE_PIN AA10 [get_ports {gpio3v3[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[1]}]

#   ---------------------------------------------------------------------------
#   -- Miscellaneous BabyWR pins GPIO 3V3
#   ---------------------------------------------------------------------------

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 26). Used for  Auxiliary I2C scl
set_property PACKAGE_PIN AA12 [get_ports {gpio3v3[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[2]}]

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 24). Used for Auxiliary I2C sda
set_property PACKAGE_PIN AB12 [get_ports {gpio3v3[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[3]}]

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 10). Not used yet.
set_property PACKAGE_PIN AB10 [get_ports {gpio3v3[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[4]}]

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 30). Not used yet.
set_property PACKAGE_PIN AB9 [get_ports {gpio3v3[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[5]}]

#   ---------------------------------------------------------------------------
#   -- Shared SPI interface to (external) DAC
#   ---------------------------------------------------------------------------

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 58). dac_refclk_sclk_o
set_property PACKAGE_PIN AB8 [get_ports {gpio3v3[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[6]}]

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 56). dac_refclk_din_o
set_property PACKAGE_PIN AA8 [get_ports {gpio3v3[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[7]}]

# Bank 84 (HD) VCCO - 3.3 V
# (M.2 pin 48). dac_refclk_cs_n_o
set_property PACKAGE_PIN Y9 [get_ports {gpio3v3[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio3v3[8]}]

#   ---------------------------------------------------------------------------
#   -- Miscellaneous BabyWR pins GPIO 1V8
#   ---------------------------------------------------------------------------

# Bank 64 (HP) VCCO - 1.8 V (M.2 pins 7, 5)
set_property PACKAGE_PIN W17 [get_ports {gpio1v8[0]}]
set_property IOSTANDARD LVDS [get_ports {gpio1v8[0]}]
set_property PACKAGE_PIN W18 [get_ports {gpio1v8[1]}]
set_property IOSTANDARD LVDS [get_ports {gpio1v8[1]}]

# Bank 64 (HD) VCCO - 1.8 V
# Reset  (M.2 pin 67)
set_property PACKAGE_PIN T20 [get_ports {gpio1v8[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {gpio1v8[2]}]

#   ---------------------------------------------------------------------------
#   -- BabyWR LEDs
#   ---------------------------------------------------------------------------

# Bank 85 (HD) VCCO - 3.3 V
# Used for led_link_o
set_property PACKAGE_PIN C10     [get_ports {led[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[0]}]

# Bank 85 (HD) VCCO - 3.3 V
# Used for led_act_o
set_property PACKAGE_PIN C11     [get_ports {led[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[1]}]

# Bank 85 (HD) VCCO - 3.3 V
# Used for led_link_o
set_property PACKAGE_PIN B9     [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[2]}]

# Bank 85 (HD) VCCO - 3.3 V
set_property PACKAGE_PIN C9     [get_ports {led[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led[3]}]

# I2C interface for accessing
# EEPROM    (24AA64       Addr 1010.000x) and
# Unique ID (24AA025EU48, Addr 1010.001x).
# Bank 64 (HD) VCCO - 1.8 V
set_property PACKAGE_PIN AB19 [get_ports scl_b]
set_property IOSTANDARD LVCMOS18 [get_ports scl_b]
set_property PACKAGE_PIN AA18 [get_ports sda_b]
set_property IOSTANDARD LVCMOS18 [get_ports sda_b]

# PPS_OUT
# Bank 64 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN AB20 [get_ports pps_p_o]
set_property IOSTANDARD LVDS  [get_ports pps_p_o]
# Bank 66 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN AB21 [get_ports pps_n_o]
set_property IOSTANDARD LVDS  [get_ports pps_n_o]

# 10MHz_out
# Bank 64 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN AA21 [get_ports clk_10m_p_o]
set_property IOSTANDARD LVDS  [get_ports clk_10m_p_o]
# Bank 64(HP) VCCO - 1.8 V
set_property PACKAGE_PIN AA22 [get_ports clk_10m_n_o]
set_property IOSTANDARD LVDS  [get_ports clk_10m_n_o]

# W.FL Connector CLK1p/n (used for ABSCAL_TXTS)
# Bank 66 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN B17 [get_ports wfl_clk1_p_o]
set_property IOSTANDARD LVDS [get_ports wfl_clk1_p_o]
# Bank 66 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN B18 [get_ports wfl_clk1_n_o]
set_property IOSTANDARD LVDS [get_ports wfl_clk1_n_o]

# PPS_IN
# Bank 66 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN C14 [get_ports pps_p_i]
set_property IOSTANDARD LVDS [get_ports pps_p_i]
# Bank 66 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN B14 [get_ports pps_n_i]
set_property IOSTANDARD LVDS [get_ports pps_n_i]

#  ------------------------------------------------------------------------------
#  -- 10MHz and PPS re-clocking Flip-Flop Resets
#  ------------------------------------------------------------------------------
# Bank 64 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN W16 [get_ports reclk_en_p_o]
set_property IOSTANDARD LVDS [get_ports reclk_en_p_o]
# Bank 64 (HP) VCCO - 1.8 V
set_property PACKAGE_PIN Y16 [get_ports reclk_en_n_o]
set_property IOSTANDARD LVDS [get_ports reclk_en_n_o]

#   ---------------------------------------------------------------------------`
#   -- FLASH PROM properties
#   ---------------------------------------------------------------------------

set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 85.0 [current_design]