-------------------------------------------------------------------------------
-- Title      : WRPC reference design for babywr
--            : based on ZYNQ Z030/Z035/Z045
-- Project    : WR PTP Core and EMPIR 17IND14 WRITE 
-- URL        : http://www.ohwr.org/projects/wr-cores/wiki/Wrpc_core
--            : http://empir.npl.co.uk/write/
-------------------------------------------------------------------------------
-- File       : babywr_ref_top.vhd
-- Author(s)  : Peter Jansweijer <peterj@nikhef.nl>
-- Company    : Nikhef
-- Created    : 2021-12-08
-- Last update: 2021-12-08
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: Top-level file for the WRPC reference design on the Baby-WR
--
-- This is a reference top HDL that instanciates the WR PTP Core together with
-- its peripherals to be run on a CLB card.
--
-- There are two main usecases for this HDL file:
-- * let new users easily synthesize a WR PTP Core bitstream that can be run on
--   reference hardware
-- * provide a reference top HDL file showing how the WRPC can be instantiated
--   in HDL projects.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2021 Nikhef
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.wr_board_pkg.all;
use work.wr_babywr_pkg.all;
use work.axi4_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity babywr_ref is
  generic (
    g_dpram_initf : string := "../../../../../sw/precompiled/wrps-sw-v5_babywr/wrc.bram";
    -- In Vivado Project-Mode, during a Synthesis run or an Implementation run, the Vivado working
    -- directory temporarily changes to the "project_name/project_name.runs/run_name" directory.

    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the testbench.
    -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
    g_simulation   : integer := 0;
    g_dac_bits     : integer := 20;
    g_ext_dac_type : string  := "MAX5719";
    g_use_pps_in   : string  := "single";
    g_ref_clk_freq : string  := "100MHz";
    g_irig_b_enable: boolean := FALSE
  );
  port (
    ---------------------------------------------------------------------------`
    -- Clocks/resets
    ---------------------------------------------------------------------------

    -- Local oscillators
    clk_125m_dmtd_p_i : in std_logic;             -- 124.992 MHz PLL reference
    clk_125m_dmtd_n_i : in std_logic;

    clk_ref_gth_n_i   : in std_logic;             -- GTH reference (100 MHz or 125MHz either from
    clk_ref_gth_p_i   : in std_logic;             -- WR scillators or stable external oscillator)

    ---------------------------------------------------------------------------
    -- SFP I/O for transceiver
    ---------------------------------------------------------------------------

    sfp_txp_o         : out   std_logic;
    sfp_txn_o         : out   std_logic;
    sfp_rxp_i         : in    std_logic;
    sfp_rxn_i         : in    std_logic;
    sfp_mod_def0_i    : in    std_logic;          -- sfp detect
    sfp_mod_def1_b    : inout std_logic;          -- scl
    sfp_mod_def2_b    : inout std_logic;          -- sda
    sfp_los_fault_i   : in    std_logic;
    sfp_tx_disable_o  : out   std_logic;

    ---------------------------------------------------------------------------
    -- GPIO on M.2. connector (i.e. UART, LED, Button, DAC etc.)
    ---------------------------------------------------------------------------
    gpio1v8 : inout  std_logic_vector(2 downto 0);
    gpio3v3 : inout  std_logic_vector(8 downto 0);
    led     : out    std_logic_vector(3 downto 0);
    
    ---------------------------------------------------------------------------
    -- No Flash memory SPI interface
    ---------------------------------------------------------------------------

    ---------------------------------------------------------------------------
    -- I2C SiTime5359 Reference & DMTD Oscillators
    ---------------------------------------------------------------------------
    refclk_sda : inout std_logic;
    refclk_scl : inout std_logic;
    dmtd_sda   : inout std_logic;
    dmtd_scl   : inout std_logic;

    ---------------------------------------------------------------------------
    -- EEPROM interface
    ---------------------------------------------------------------------------
    -- I2C interface for accessing
    -- EEPROM    (24AA64       Addr 1010.000x) and
    -- Unique ID (24AA025EU48, Addr 1010.001x).
    scl_b : inout std_logic;
    sda_b : inout std_logic;
    
    ---------------------------------------------------------------------------
    -- PCIe interface
    ---------------------------------------------------------------------------

    pci_clk_n          : in  std_logic;
    pci_clk_p          : in  std_logic;
    perst_n            : in  std_logic;
    pcie_rxp           : in  std_logic;
    pcie_rxn           : in  std_logic;
    pcie_txp           : out std_logic;
    pcie_txn           : out std_logic;

    ------------------------------------------------------------------------------
    -- Digital I/O 
    ------------------------------------------------------------------------------
    pps_p_o         : out std_logic;
    pps_n_o         : out std_logic;
    wfl_clk1_p_o    : out std_logic;
    wfl_clk1_n_o    : out std_logic;
    clk_10m_p_o     : out std_logic;
    clk_10m_n_o     : out std_logic;
    reclk_en_p_o    : out std_logic;
    reclk_en_n_o    : out std_logic;
    
    pps_p_i         : in  std_logic;
    pps_n_i         : in  std_logic;
    clk_ext_10m_p_i : in  std_logic;
    clk_ext_10m_n_i : in  std_logic
  );
end entity babywr_ref;

architecture top of babywr_ref is

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------

  -- Number of masters on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 2;

  -- Number of slaves on the primary wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 1;

  -- Primary Wishbone master(s) offsets
  constant c_WB_MASTER_PCIE    : integer := 0;
  constant c_WB_MASTER_ETHBONE : integer := 1;

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_WRC : integer := 0;

  -- sdb header address on primary crossbar
  constant c_SDB_ADDRESS : t_wishbone_address := x"00040000";

  -- f_xwb_bridge_manual_sdb(size, sdb_addr)
  -- Note: sdb_addr is the sdb records address relative to the bridge base address
  constant c_wrc_bridge_sdb : t_sdb_bridge :=
    f_xwb_bridge_manual_sdb(x"0003ffff", x"00030000");

  -- Primary wishbone crossbar layout
  constant c_WB_LAYOUT : t_sdb_record_array(c_NUM_WB_SLAVES - 1 downto 0) := (
    c_WB_SLAVE_WRC => f_sdb_embed_bridge(c_wrc_bridge_sdb, x"00000000"));

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- clock and reset
  signal clk_sys_62m5    : std_logic;
  signal rst_sys_62m5_n  : std_logic;
  signal rst_ref_62m5_n  : std_logic;
  signal clk_ref_62m5    : std_logic;
  signal clk_10m_out     : std_logic;
  signal clk_10m_oddr    : std_logic;
  signal clk_100m_out    : std_logic;
  signal clk_100m_oddr   : std_logic;
  signal clk_500m        : std_logic;
  signal clk_ext_10m     : std_logic;

  -- DAC signals for reference clock
  signal dac_refclk_sclk_int_o  : std_logic;
  signal dac_refclk_din_int_o   : std_logic;
  signal dac_refclk_cs_n_int_o  : std_logic;

  -- SFP
  signal sfp_sda_in  : std_logic;
  signal sfp_sda_out : std_logic;
  signal sfp_scl_in  : std_logic;
  signal sfp_scl_out : std_logic;

  -- LEDs and GPIO
  signal wrc_abscal_txts_out  : std_logic;
  signal wrc_abscal_txts_oddr : std_logic;
  signal wrc_abscal_rxts_out  : std_logic;
  signal wrc_pps_out          : std_logic;
  signal wrc_pps_oddr         : std_logic;
  signal wrc_pps_led          : std_logic;
  signal pps_led_ext          : std_logic;
  signal svec_led             : std_logic_vector(15 downto 0);
  signal wrc_pps_in           : std_logic;
  signal spare_io             : std_logic_vector(1 downto 0);

  --Axi4
  signal m_axil_i : t_axi4_lite_master_in_32;
  signal m_axil_o : t_axi4_lite_master_out_32;

  --TAI
  signal tm_time_valid : std_logic;
  signal tm_tai : std_logic_vector(39 downto 0);
  signal irigb : std_logic;

  --Wishbone
  signal wb_master_i : t_wishbone_master_in; 
  signal wb_master_o : t_wishbone_master_out;

  --PCIe 
  signal pci_clk : std_logic;

  component pll_62m5_500m is
    port (
      areset_n_i        : in  std_logic;
      clk_62m5_pllref_i : in  std_logic;             
      clk_500m_o        : out std_logic;
      pll_500m_locked_o : out std_logic
    );
  end component pll_62m5_500m;

  component gen_x_mhz is
    generic (
      g_divide    : integer
    );
    port (
      clk_500m_i  : in  std_logic;
      rst_n_i     : in  std_logic;
      pps_i       : in  std_logic;
      clk_x_mhz_o : out std_logic
    );
  end component gen_x_mhz;

begin  -- architecture top

  ---------------------------------------------------------------------------
  -- PCIe interface
  ---------------------------------------------------------------------------
  Pcie: component pcie_1x
     port map (
      M00_AXI_0_araddr  => m_axil_o.araddr,
      M00_AXI_0_arburst => open,
      M00_AXI_0_arcache => open,
      M00_AXI_0_arlen   => open,
      M00_AXI_0_arlock  => open,
      M00_AXI_0_arprot  => open,
      M00_AXI_0_arqos   => open,
      M00_AXI_0_arready => m_axil_i.arready,
      M00_AXI_0_arsize  => open,
      M00_AXI_0_arvalid => m_axil_o.arvalid,
      M00_AXI_0_awaddr  => m_axil_o.awaddr,
      M00_AXI_0_awburst => open,
      M00_AXI_0_awcache => open,
      M00_AXI_0_awlen   => open,
      M00_AXI_0_awlock  => open,
      M00_AXI_0_awprot  => open,
      M00_AXI_0_awqos   => open,
      M00_AXI_0_awready => m_axil_i.awready,
      M00_AXI_0_awsize  => open,
      M00_AXI_0_awvalid => m_axil_o.awvalid,
      M00_AXI_0_bready  => m_axil_o.bready,
      M00_AXI_0_bresp   => m_axil_i.bresp,
      M00_AXI_0_bvalid  => m_axil_i.bvalid,
      M00_AXI_0_rdata   => m_axil_i.rdata,
      M00_AXI_0_rlast   => m_axil_i.rlast,
      M00_AXI_0_rready  => m_axil_o.rready,
      M00_AXI_0_rresp   => m_axil_i.rresp,
      M00_AXI_0_rvalid  => m_axil_i.rvalid,
      M00_AXI_0_wdata   => m_axil_o.wdata,
      M00_AXI_0_wlast   => m_axil_o.wlast,
      M00_AXI_0_wready  => m_axil_i.wready,
      M00_AXI_0_wstrb   => m_axil_o.wstrb,
      M00_AXI_0_wvalid  => m_axil_o.wvalid,
      aclk1             => clk_sys_62m5,
      pci_clk_n(0)      => pci_clk_n,
      pci_clk_p(0)      => pci_clk_p,
      pcie_rxn(0)       => pcie_rxn,
      pcie_rxp(0)       => pcie_rxp,
      pcie_txn(0)       => pcie_txn,
      pcie_txp(0)       => pcie_txp,
      perst_n           => perst_n
    );

  -----------------------------------------------------------------------------
  -- Axi to Wishbone converter
  -----------------------------------------------------------------------------

AXI2WB : xwb_axi4lite_bridge 
  port map(
    clk_sys_i => clk_sys_62m5,
    rst_n_i   => rst_sys_62m5_n,
                
    axi4_slave_i => m_axil_o,
    axi4_slave_o => m_axil_i,
    wb_master_o  => wb_master_o,
    wb_master_i  => wb_master_i  
  );

  -----------------------------------------------------------------------------
  -- Irig_B converter
  -----------------------------------------------------------------------------
gen_irig_b : if (g_irig_b_enable = TRUE) generate
  IRIG_B : wr_irigb_conv
    generic map(
      clk_freq => 62500000
    )
    port map(
      clk_i           => clk_ref_62m5,
      rst_n_i         => rst_ref_62m5_n,
      pps_i           => wrc_pps_out,
      irig_b_o        => irigb,
      tm_time_valid_i => tm_time_valid,
      tm_tai_i        => tm_tai
    );
end generate gen_irig_b;

  -----------------------------------------------------------------------------
  -- The WR PTP core board package (WB Slave + WB Master)
  -----------------------------------------------------------------------------

  cmp_xwrc_board_babywr : xwrc_board_babywr
    generic map (
      g_simulation                => g_simulation,
      g_with_external_clock_input => TRUE,
      g_dpram_initf               => g_dpram_initf,
      g_fabric_iface              => PLAIN,
      g_dac_bits                  => g_dac_bits,
      g_ext_dac_type              => g_ext_dac_type,
      g_ref_clk_freq              => g_ref_clk_freq)
    port map (
      areset_n_i          => gpio1v8(2),
      clk_125m_dmtd_n_i   => clk_125m_dmtd_n_i,
      clk_125m_dmtd_p_i   => clk_125m_dmtd_p_i,
      clk_ref_gth_n_i     => clk_ref_gth_n_i,
      clk_ref_gth_p_i     => clk_ref_gth_p_i,
      clk_10m_ext_i       => clk_ext_10m,
      clk_sys_62m5_o      => clk_sys_62m5,
      clk_ref_62m5_o      => clk_ref_62m5,
      rst_sys_62m5_n_o    => rst_sys_62m5_n,
      rst_ref_62m5_n_o    => rst_ref_62m5_n,

      sfp_txp_o           => sfp_txp_o,
      sfp_txn_o           => sfp_txn_o,
      sfp_rxp_i           => sfp_rxp_i,
      sfp_rxn_i           => sfp_rxn_i,
      sfp_det_i           => sfp_mod_def0_i,
      sfp_sda_i           => sfp_sda_in,
      sfp_sda_o           => sfp_sda_out,
      sfp_scl_i           => sfp_scl_in,
      sfp_scl_o           => sfp_scl_out,
      sfp_rate_select_o   => open,
      sfp_tx_fault_i      => sfp_los_fault_i,
      sfp_tx_disable_o    => sfp_tx_disable_o,
      sfp_los_i           => sfp_los_fault_i,

      refclk_sda          => refclk_sda,
      refclk_scl          => refclk_scl,
      dmtd_sda            => dmtd_sda,
      dmtd_scl            => dmtd_scl,

      dac_refclk_cs_n_o   => gpio3v3(8),
      dac_refclk_sclk_o   => gpio3v3(6),
      dac_refclk_din_o    => gpio3v3(7),

      eeprom_scl          => scl_b,
      eeprom_sda          => sda_b,

      aux_scl             => gpio3v3(2),
      aux_sda             => gpio3v3(3),

      -- Uart
      uart_rxd_i          => gpio3v3(0),
      uart_txd_o          => gpio3v3(1),

      -- Spare Outputs
      spare_io            => spare_io,

      -- Wishbone
      wb_slave_i          => wb_master_o,
      wb_slave_o          => wb_master_i,
      
      abscal_txts_o       => wrc_abscal_txts_out,
      abscal_rxts_o       => wrc_abscal_rxts_out,

      -- TAI
      tm_link_up_o        => open,
      tm_time_valid_o     => tm_time_valid,
      tm_tai_o            => tm_tai,
      tm_cycles_o         => open,

--      pps_csync_o         => gpio3v3(4),
      pps_csync_o         => open,
      pps_ext_i           => wrc_pps_in,
      pps_p_o             => wrc_pps_out,
      pps_led_o           => wrc_pps_led,
      led_link_o          => led(0),
      led_act_o           => led(1));

  -- Tristates for SFP EEPROM
  sfp_mod_def1_b <= '0' when sfp_scl_out = '0' else 'Z';
  sfp_mod_def2_b <= '0' when sfp_sda_out = '0' else 'Z';
  sfp_scl_in     <= sfp_mod_def1_b;
  sfp_sda_in     <= sfp_mod_def2_b;

  -- Not (yet) used.

gen_irig_b_o : if (g_irig_b_enable = TRUE) generate
  gpio3v3(4) <= irigb;
end generate gen_irig_b_o;

gen_no_irig_b_o : if (g_irig_b_enable = FALSE) generate
  gpio3v3(4) <= spare_io(0);
end generate gen_no_irig_b_o;

  gpio3v3(5) <= spare_io(1);
  led(3)     <= '0';

  ------------------------------------------------------------------------------
  -- 10MHz output generation
  ------------------------------------------------------------------------------
  -- A 500 MHz reference clock is necessary since:
  -- 10 MHz = 50 ns '1', 50 ns '0' (25 ticks '1', 25 ticks '0', duty cycle 50%)
  -- 100 MHz = 5  ns '1', 5  ns '0' (4 ticks '1' , 6  ticks '0', duty cycle 40%) 
  cmp_pll_62m5_500m: pll_62m5_500m
    port map (
      areset_n_i        => rst_ref_62m5_n,
      clk_62m5_pllref_i => clk_ref_62m5,
      clk_500m_o        => clk_500m,
      pll_500m_locked_o => open
    );

-------------------------------------
-- 10 MHz generation, ODDR and OBUFDS
-------------------------------------
  cmp_gen_10_mhz: gen_x_mhz
    generic map (
      g_divide => 50
    )
    port map (
      clk_500m_i  => clk_500m,
      rst_n_i     => rst_ref_62m5_n,
      pps_i       => wrc_pps_out,
      clk_x_mhz_o => clk_10m_out
    );

  oddr_10m: ODDRE1
    port map(
      Q  => clk_10m_oddr,
      C  => clk_500m,
      D1 => clk_10m_out,
      D2 => clk_10m_out,
      SR => '0');
  
  cmp_obuf_be_10mhz_out : OBUFDS
    port map (
      I  => clk_10m_oddr,
      O  => clk_10m_p_o,
      OB => clk_10m_n_o);

--------------------------------------
-- 100 MHz generation, ODDR and OBUFDS
--------------------------------------
  cmp_gen_100mhz: gen_x_mhz
    generic map (
      g_divide => 5     -- 500/5 MHz = 100 MHz (40% duty cycle!)
    )
    port map (
      clk_500m_i  => clk_500m,
      rst_n_i     => rst_ref_62m5_n,
      pps_i       => wrc_pps_out,
      clk_x_mhz_o => clk_100m_out
    );

  oddr_100m: ODDRE1
    port map(
      Q  => clk_100m_oddr,
      C  => clk_500m,
      D1 => clk_100m_out,
      D2 => clk_100m_out,
      SR => '0');
  
  cmp_obuf_be_100mhz_out : OBUFDS
    port map (
      I  => clk_100m_oddr,
      O  => gpio1v8(0),
      OB => gpio1v8(1));

  ------------------------------------------------------------------------------
  -- 10MHz and PPS re-clocking Flip-Flop Resets
  ------------------------------------------------------------------------------
  cmp_obuf_reclk_en_out : OBUFDS
    port map (
      I  => '1',            -- Re-clocking always enabled
      O  => reclk_en_p_o,
      OB => reclk_en_n_o);

  ------------------------------------------------------------------------------
  -- LEDs
  ------------------------------------------------------------------------------
  U_Extend_PPS : gc_extend_pulse
  generic map (
    g_width => 10000000)
  port map (
    clk_i      => clk_ref_62m5,
    rst_n_i    => rst_ref_62m5_n,
    pulse_i    => wrc_pps_led,
    extended_o => pps_led_ext);

  led(2)     <= pps_led_ext;

  ------------------------------------------------------------------------------
  -- Common FMC pin mapping Reference Design (babywr_ref_top)
  ------------------------------------------------------------------------------
  -- Configure DIO Output Enable
  -- Configure Digital I/Os 0 to 3 as outputs

  ----------------------
  -- PPS ODDR and OBUFDS
  ----------------------
  oddr_pps: ODDRE1
    port map(
      Q  => wrc_pps_oddr,
      C  => clk_500m,
      D1 => wrc_pps_out,
      D2 => wrc_pps_out,
      SR => '0');
  
  U_obuf_dio_o_0 : OBUFDS
    port map (
      I  => wrc_pps_oddr,
      O  => pps_p_o,
      OB => pps_n_o);

  ---------------------------------------
  -- wrc_abscal_txts_oddr ODDR and OBUFDS
  ---------------------------------------
  oddr_wrc_abscal_txts: ODDRE1
    port map(
      Q  => wrc_abscal_txts_oddr,
      C  => clk_500m,
      D1 => wrc_abscal_txts_out,
      D2 => wrc_abscal_txts_out,
      SR => '0');
  
  U_obuf_dio_o_1 : OBUFDS
    port map (
      I  => wrc_abscal_txts_oddr,
      O  => wfl_clk1_p_o,
      OB => wfl_clk1_n_o);
      
  -- Grand Master inputs
  U_ibuf_dio_i_3: IBUFDS
    generic map (
      DIFF_TERM => true)
    port map (
      O  => wrc_pps_in,
      I  => pps_p_i,
      IB => pps_n_i);

  cmp_ibugds_extref: IBUFGDS
    generic map (
      DIFF_TERM => true)
    port map (
      O  => clk_ext_10m,
      I  => clk_ext_10m_p_i,
      IB => clk_ext_10m_n_i);

end architecture top;
