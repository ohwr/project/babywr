rem prog.cmd  PeterJ,    08-Dec-2021.
@prompt $$$s

rem ### Clean up old log files and stuff
del vivado_gen_bin_mcs.log

rem ### note that environment variable "VIVADO" must be set to something like "E:\Xilinx\Vivado\2017.1\bin\"
rem ### in your (User) Environment Variables
"%VIVADO%\vivado.bat" -mode batch -source ..\..\..\sw\scripts\viv_gen_bin_mcs.tcl -log vivado_gen_bin_mcs.log
