board  = "babywr"
target = "xilinx"
action = "synthesis"

syn_device = "xcau15p"
syn_grade = "-1"
syn_package = "sbvb484"

# PHY Reference Clock Frequency defaults to 125 MHz
# if "phy_ref_clk" is defined and "100" then select GTHE4-lp 100 MHz files
phy_ref_clk = "100"

syn_top = "babywr_ref"
syn_project = "babywr_ref.xpr"

syn_tool = "vivado"

modules = { "local" : "../../top/babywr_ref_design/"}