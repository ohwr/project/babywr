#
# projetc_properties.tcl
# This file contains the general project properties such as the project name and
# the directory where Vivado is doing it's job
#
# ====================================================

# ====================================================
# SELECT DESIGN TO BUILD:
# ====================================================
# Select BabyWR module version:
# either "2260-D6-M"
# or "2280-D6-M", with re-clock (must implement lock-sweep!)
set version "2260-D6-M"
#set version "2280-D6-M"

# ====================================================
# SELECT DEVICE TO BUILD:
# ====================================================
# Baby WR equipped with Artix UltraScale+ (speed grade -1 has lowest performance)
set proj_device xcau10p-sbvb484-1L-i
#set proj_device xcau15p-sbvb484-1-e

# ====================================================
# SELECT REFERCE OSCILLATOR FREQUENCY:
# ====================================================
# Baby WR X1 default 100 MHz
# Choose "125MHz" when using 100MHzInTheLoop
#set ref_clk_freq "100MHz"
set ref_clk_freq "125MHz"
# ====================================================

# ====================================================
# SELECT REFERCE OSCILLATOR LOCAL/EXTERNAL:
# ====================================================
# Default local (i.e, SiTime5359)
# Choose "external" when using for example 100MHzInTheLoop
set ref_oscillator "local"
#set ref_oscillator "external"
# ====================================================

set dac_bits 20
set ext_dac_type "MAX5719"

# ====================================================
# ENABLE IRIG_B OUTPUT  (DEFAULT FALSE):
# ====================================================
# Uncomment the line below to enable IRIG_B on gpio_3V3(4)
set irig_b_enable TRUE
# ====================================================

# ====================================================
# ====================================================
# Use proper fileset for 100/125 MHz (default = 100 MHz)
# Note that selected files need to have the same verilog module names
# since these are Xilinx gthe4 wizard files. Therefor selection via a
# generic in vhdl is difficult.
if {$ref_clk_freq == "125MHz"} {
  file copy -force proj_file_list_125MHz.txt proj_file_list.txt
} else {
  file copy -force proj_file_list_100MHz.txt proj_file_list.txt
}

set vivbool_irig_b_enable 1'b0
if [info exists irig_b_enable] {
  if ($irig_b_enable) {
    set vivbool_irig_b_enable 1'b1
  }
}

set proj_name babywr_ref
set proj_dir work
set script_dir [pwd]/../../../sw/scripts
set upgradeip true
set wrpc_cpu_initf [pwd]/../../../sw/precompiled/wrps-sw-v5_babywr/wrc.bram
set wrpc_cpu_elf [pwd]/../../../sw/precompiled/wrps-sw-v5_babywr/wrc.elf
set wrpc_cpu_instpath "wrpc_cpu_memory"

# update revision except when argument "no_update_revison" is passed (as for example by viv_do_programm.tcl)
if {$argc == 0 || $argv != "no_update_revision"} {
  source $script_dir/revisiondate.tcl

  set generics "g_dpram_initf=$wrpc_cpu_initf \
                g_dac_bits=$dac_bits \
                g_ext_dac_type=$ext_dac_type \
                g_ref_clk_freq=$ref_clk_freq \
                g_irig_b_enable=$vivbool_irig_b_enable"
}
