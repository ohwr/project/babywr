#
# projetc_properties.tcl
# This file contains the general project properties such as the project name and
# the directory where Vivado is doing it's job
#
# ====================================================

# ====================================================
# SELECT DESIGN TO BUILD:
# ====================================================
set version ""
# Uncomment the line below for older BabyWRr V2.0
#set version "_v2"

# ====================================================
# SELECT DEVICE TO BUILD:
# ====================================================
# Baby WR equipped with Artix UltraScale+ (speed grade -1 has lowest performance)
#set proj_device xcau15p-ubva368-1-e
#set proj_device xcau15p-sbvb484-1-e
#set proj_device xcau15p-ffvb676-1-e
# Samples:
set proj_device xcau10p-sbvb484-1L-i
# ====================================================

set proj_name babywr_blink
set proj_dir work
set script_dir [pwd]/../../../sw/scripts
set upgradeip false
set generics ""
