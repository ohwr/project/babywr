rem do_vivado_tcl.cmd  PeterJ,    08-Dec-2021.
@prompt $$$s

rem ### Usually one wants to start Vivado in tcl mode to inspect an existing design,
rem ### therefore don't delete log files and setting. Else remove "rem" statements in the lines below.
rem set DesName=clbv3_wr_ref_top

rem set LogName=%DesName%-vivado

rem ### Cleanup old log files and stuff (
rem del vivado*.log
rem del vivado*.jou

rem ### note that environment variable "VIVADO" must be set to something like "E:\Xilinx\Vivado\2017.1\bin\"
rem ### in your (User) Environment Variables
"%VIVADO%\vivado.bat" -mode tcl
