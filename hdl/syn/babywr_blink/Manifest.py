board  = "babywr"
target = "xilinx"
action = "synthesis"

syn_device = "xcau15p"
syn_grade = "-1"
syn_package = "sbvb484"

syn_top = "babywr_blink"
syn_project = "babywr_blink.xpr"

syn_tool = "vivado"

modules = { "local" : "../../top/babywr_blink/"}