rem prog.cmd  PeterJ,    08-Dec-2021.
@prompt $$$s

rem ### Cleanup old log files and stuff
del vivado_prog.log

rem ### note that environment variable "VIVADO" must be set to something like "E:\Xilinx\Vivado\2017.1\bin\"
rem ### in your (User) Environment Variables
"%VIVADO%\vivado.bat" -mode batch -source ..\..\..\sw\scripts\viv_do_program.tcl -log vivado_prog.log
