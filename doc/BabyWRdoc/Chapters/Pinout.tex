\section{Connector pinout}
\label{sec:Pinout} 
This section describes the connectors and test pads on the BabyWR.

\subsection{M.2 connector}
The BabyWR module has form-factor \textquote{type 2260-D6-M} according to the PCI Express M.2 Specification ~\cite{PCIexpress_M2}.
This means that it is a 22 mm wide, 60 mm long module.
Its maximum height on top and bottom is 3.2 mm and 1.5 mm respectively.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./pictures/Inkscape/M2_Connector.pdf}
	\caption{BabyWR M.2 connector pinout. The middle columns are for reference and refer to the \textquote{Socket 3 PCIe-based Adapter Pinouts (Key M)} definition in \textquote{PCI Express M.2 Specification} ~\cite{PCIexpress_M2}.}
	\label{fig:M2Pinout}
\end{figure}

\subsubsection{BabyWR M.2 signal description}

The pinout of the connector is based on the \textquote{Socket 3 PCIe-based Adapter Pinouts (Key M)} pinout.
The BabyWR M.2 pinout definition is derived from this original pinout specification.
The signals on the BabyWR M.2 connector are assigned such that there are no conflicts for power supply pins, inputs/outputs and signal definition voltage levels with respect to the original specification.
This means that if, by accident, a BabyWR module is plugged into the SSD slot of a motherboard (it is not designed for this function!) then this will not cause damage to the motherboard, nor to the BabyWR module itself.

The middle two columns of figure ~\ref{fig:M2Pinout} show the original \textquote{Socket 3 PCIe-based Adapter Pinouts (Key M)} definition.
The middel colums are just shown for reference.
Columns 2 and 5 show the corresponding signal definitions for the BabyWR M.2 connector.

Several groups of signals can be distinguished.
Table ~\ref{tab:Signal_groups_and_definitions} lists signal names by function and defines their direction with respect to the BabyWR module.
Signals with \textquote{3V3} in their name use 3V3 signaling.
Signals with \textquote{1V8} in their name use 1V8 signaling.

The BabyWR reference design outputs a 10 MHz WR phase aligned clock (WRCLK\textunderscore p\textunderscore o, WRCLK\textunderscore n\textunderscore o).
%Within certain criteria, other frequencies (e.g., 100 MHz) are possible based on different firmware.

\begin{table}[H]
    \begin{center}
        \begin{tblr}{|c|c|c|c|}
            \hline signal & direction & function & group \\ 
            \hline {REFCLKp\\REFCLKn} & in & 100 MHz PCIe reference clock & PCIe\\ 
            \hline {PERp0\\PERn0} & out & {connect to the receiver differential\\pair on the system board} & PCIe\\ 
            \hline {PETp0\\PETRn0} & in & {connect to the transmitter differential\\pair on the system board} & PCIe\\ 
            \hline T\textunderscore P, T\textunderscore N & out & transmitter differential pair & SFP\\ 
            \hline R\textunderscore P, R\textunderscore N & in  & receiver differential pair & SFP\\ 
            \hline LOS\textunderscore Fault3V3 & in & transceiver Rx\textunderscore LOS or Tx\textunderscore Fault & SFP\\ 
            \hline Mod\textunderscore ABS1V8 & in & transceiver module absent & SFP\\ 
            \hline SFP\textunderscore SCL1V8 & out & transceiver I2C interface, SCL & SFP\\ 
            \hline SFP\textunderscore SDA1V8 & in/out & transceiver I2C interface, SDA & SFP\\ 
            \hline {WRCLK\textunderscore p\textunderscore o\\WRCLK\textunderscore n\textunderscore o} & out & {WR phase aligned clock\\(10MHz)} & WR timing\\ 
            \hline {PPS\textunderscore p\textunderscore o\\PPS\textunderscore n\textunderscore o} & out & WR phase aligned PPS & WR timing\\ 
            \hline {EXTREFCLK\textunderscore p\textunderscore o\\EXTREFCLK\textunderscore n\textunderscore o} & in & {Optional 125 MHz clock inputs\\when using an external oscillator} & WR timing\\ 
            \hline GPIO1V8\textunderscore [2:0] & in/out & 1.8 Volt general purpose IO & GPIO\\ 
            \hline GPIO3V3\textunderscore [8:0] & in/out & 3.3 Volt general purpose IO & GPIO\\ 
            \hline {TDI,TDO,\\TCK,TMS} & in/out & 3.3 Volt JTAG interface & JTAG\\ 
            \hline 
        \end{tblr}
        \caption{Signal groups and definitions. }
        \label{tab:Signal_groups_and_definitions}
    \end{center}
\end{table}

\subsubsection{Reference design GPIO1V8 definitions}

Table ~\ref{tab:GPIO1V8_definitions} lists the GPIO1V8 signals which are in use by the current BabyWR reference design.

The usual WR aligned 10 MHz clock is output via the signals WRCLK\textunderscore p\textunderscore o and WRCLK\textunderscore n\textunderscore o.
The BabyWR reference design also outputs a differential WR aligned 100 MHz via GPIO1B8(0) and GPIO1B8(1).
Note that the duty cycle of this signal is 40\%.

The BabyWR reference design van be reset by pulling GPIO1V8(2) low.

\begin{table}[H]
	\begin{center}
		\begin{tblr}{|c|c|c|}
			\hline signal & direction & function \\ 
			\hline GPIO1V8\textunderscore 0 & out & WRCLK 100 MHz\textunderscore p \\ 
			\hline GPIO1V8\textunderscore 1 & out & WRCLK 100 MHz\textunderscore n \\ 
			\hline GPIO1V8\textunderscore 2 & in  & Reset\textunderscore n \\ 
			\hline 
		\end{tblr}
		\caption{GPIO1V8 signals used in the reference design. }
		\label{tab:GPIO1V8_definitions}
	\end{center}
\end{table}

\subsubsection{Reference design GPIO3V3 definitions}

Table ~\ref{tab:GPIO3V3_definitions} lists the GPIO3V3 signals which are in use by the current BabyWR reference design.
The UART signals connect to the WR terminal that is running on the WRPC RISC-V core.
An auxiliary I2C interface can be used access more input output features via an I2C IO port (e.g., PCA 9554 ~\cite{PCA9554}). 

\begin{table}[H]
    \begin{center}
        \begin{tblr}{|c|c|c|}
            \hline signal & direction & function \\ 
            \hline GPIO3V3\textunderscore 0 & in     & UART RxD \\ 
            \hline GPIO3V3\textunderscore 1 & out    & UART TxD \\ 
            \hline GPIO3V3\textunderscore 2 & out    & Aux I2C SCL \\ 
            \hline GPIO3V3\textunderscore 3 & in/out & Aux I2C SDA \\ 
            \hline GPIO3V3\textunderscore 4 & out    & IRIGB \\ 
            \hline GPIO3V3\textunderscore 5 & out    & Debug \\ 
            \hline GPIO3V3\textunderscore 6 & out    & DAC SPI SCLK \\ 
            \hline GPIO3V3\textunderscore 7 & out    & DAC SPI DIN \\ 
            \hline GPIO3V3\textunderscore 8 & out    & DAC SPI SYNC\textunderscore n \\ 
            \hline 
		\end{tblr}
		\caption{GPIO3V3 signals used in the reference design. }
		\label{tab:GPIO3V3_definitions}
	\end{center}
\end{table}


\subsection{U.FL coaxial connectors}
BabyWR 2260-D6-M has 4 U.FL coaxial connectors.
J2 and J3 (Clk0, p/n) can be used \footnote{Not implemented in the reference design} as input for an external reference clock.
J4 and J5 (Clk1, p/n) can be used as general purpose IO or absolute calibration\footnote{The reference design outputs LVDS \textquote{abscal\_txts}}.

\subsection{Test pads}
BabyWR 2260-D6-M has 4 test pads that connect to FPGA pins on Bank 66 (1V8) according to Table ~\ref{tab:TestPad_definitions}.
BabyWR is designed to be a WR-Node; i.e., it is a timeReceiver.
However, BabyWR can also be used as free running timeTransmitter or as Grand-Master.
In the exceptional case one would like to use BabyWR as a Grand-Master then test pads TP1-TP4 enable the possibility to do so by connecting the test pads to an external (LVDS) 10MHz and 1 PPS reference.

\begin{table}[H]
	\begin{center}
		\begin{tblr}{|c|c|c|}
			\hline signal & FPGA pin & function \\ 
			\hline TP1 & C19 & 10MHz\_p\_i \\ 
			\hline TP2 & B19 & 10MHz\_n\_i \\ 
			\hline TP3 & C14 & PPS\_p\_i \\ 
			\hline TP4 & B14 & PPS\_n\_i \\ 
			\hline 
		\end{tblr}
		\caption{Test pad LVDS signals used in the reference design.}
		\label{tab:TestPad_definitions}
	\end{center}
\end{table}

\nomenclature{NC}{Not Connected.}

\newpage