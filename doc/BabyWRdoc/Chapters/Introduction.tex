\section{Introduction}

BabyWR~\cite{BabyWR} is a simple White Rabbit node.
It is designed to operate as timeReceiver.
%(\textcolor{red}{although it can also operate as timeTransmiter or Grand-Master, see Appendix ~\cite{}}).
The propose of BabyWR is to:
\begin{itemize}
	\item be a WR-Node timeReciever that supplies a 1PPS and 10 MHz (or 100 MHz) signal,
	\item with good quality (Low Phase Noise; $f_{c}$ 10 MHz $<$ 100 dBc{/}Hz @ 10 Hz) on board clocks,
	\item with an option to use an external high precision OCXO as local reference oscillator, for extreme low phase noise applications.
	\item Supply 12 General Purpose Input Output (GPIO) pins.
	\item Low power ($<$ 1.6 W).
	\item Small and cost effective.	
\end{itemize}

The form-factor of the BabyWR module is PCI Express M.2 Type 2260-D6-M, see ~\cite{PCIexpress_M2}.

BabyWR is supposed to be implemented on a main board that will host an Small Form-factor Plugable (SFP) module ~\cite{SFP} which connects to a White Rabbit network.
This enables flexibility with respect to the placement of the SFP module since a main board usually is built into a mechanical enclosure and the SFP module will be placed such that it aligns with a front-panel.

\newpage
\subsection{Minimal BabyWR implementation }
\label{sec:BabyWR_Minimal}

%\footnote{White Rabbit Absolute Calibration Procedure

\begin{figure}[H]
	 	\centering
	 	\includegraphics[width=\textwidth]{./pictures/Inkscape/BabyWR_2260-D6-M_Minimal.pdf}
	 	\caption{Minimal BabyWR implementation.} %{see footnote\protect\footnotemark}
        \label{fig:BabyWR_Minimal}
\end{figure}
\noindent

Figure ~\ref{fig:BabyWR_Minimal} shows the minimal BabyWR implementation.
\newline

The following items are necessary:
\begin{itemize}
	\item The module must be supplied with 3.3 volt.
	\item An SFP module, placed on the main board, must be connected.
    \nomenclature{SFP}{Small Form-factor Pluggable.}
\end{itemize}

The following items are advisable:
\begin{itemize}
	\item A reset facility, for example a \textquote{Reset Button}.
	\item When no FPGA configuration image is stored in FLASH memory then a Xilinx download cable JTAG connection is mandatory in order to be able to program the FPGA and/or FLASH memory.
	\item A connection to the WR terminal can be established via a UART connection.
	It may be convenient to implement a USB$/$UART bridge device on the main board (e.g, Silicon Labs CP2105 ~\cite{CP2105}).
\end{itemize}

\subsection{Using an external reference oscillator }
\label{sec:BabyWR_External}

BabyWR has an excellent on board SiTime SIT5359 local reference oscillator.
Unfortunately the quality of the WR timing signals is deteriorated by the phase noise of the FPGA.
For extreme low phase noise levels, one can replace the local reference oscillator in the WR loop by an external high precision oscillator (OCXO).
Figure ~\ref{fig:BabyWR_External} shows a BabyWR implementation that uses an external oscillator.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{./pictures/Inkscape/BabyWR_2260-D6-M_Minimal_ext1_2.pdf}
	\caption{BabyWR using a (high precision) external oscillator.} %{see footnote\protect\footnotemark}
	\label{fig:BabyWR_External}
\end{figure}
\noindent
	
The external OCXO can is tuned by a DAC that is driven by an SPI bus which is galvanic isolated to avoid insertion of digital noise in the oscillator power supply system.
The output of the OCXO is fed into a PLL to create clock a 125 MHz WR reference clock that is used to close the WR loop.

\subsubsection{Considerations when using an external oscillator}
Be aware that the PLL in this scheme will lock on an arbitrary phase offset when the local reference oscillator frequency differs from the 125 MHz WR reference frequency.
BabyWR uses a 16-bit PHY so its WR reference clock is 62.5 MHz.
The 62.5 MHz WR reference clock will be aligned to the timeTransmitter reference clock.
Hence, at startup or link reset, the external oscillator arbitrarily locks at multiple phase offsets depending on the frequency difference of the external oscillator with respect to the 62.5 MHz WR reference clock.
The number of possible phase offsets is defined by the Least Common Multiple (LCM) of the respective clock periods of the WR refence clock ($t_{WRref}$) and the external oscillator ($t_{ext}$ ns). 
\nomenclature{LCM}{Least Common Multiplier.}
\begin{equation}
	N_{\phi}=\frac{LCM(t_{WRref},t_{ext})}{t_{WRreF}}
	\label{eq:amount_of_possible_phase_offsets}
\end{equation}
\noindent
When, for example, the frequency of the external oscillator is 100 MHz, then the external oscillator can lock at 5 distinct phase offsets positions with respect to the WR reference clock.

There are two options to mitigate this issue:
\begin{itemize}
	\item Reclock the WR timing signals (see ~\ref{sec:reclock_WR_timing_signals}),
	\item implement \textquote{Lock Sweep} (see ~\ref{sec:lock_sweep}).
\end{itemize}
\noindent
Both options will be described in the following sub sections.


\paragraph{Reclock WR timing signals using the external oscillator}
\label{sec:reclock_WR_timing_signals}
\indent
\\
\indent
The WR timing signals (usually 1PPS and 10 MHz) which are aligned to the timeTransmitter clock, are generated by the WR implementation on the FPGA.
The phase noise quality of these signals is deteriorated by the FPGA.
However, these signals can be reclocked by the \textquote{clean} external oscillator.
When the external oscillator can lock at multiple phase offsets (see section  ~\ref{sec:BabyWR_External} above) then the WR timing signals can be reclocked by an integer multiple \textquote{$n$} of the Least Common Multiple (LCM) of the external oscillator frequency and the 62.5 MHz WR reference clock (see equation~\eqref{eq:reclock_freq}; frequencies in MHz).
\nomenclature{LCM}{Least Common Multiple.}
\begin{equation}
	f_{reclock} = n*LCM(f_{ext},f_{WRref})
	\label{eq:reclock_freq}
\end{equation}
\noindent
When, for example, the frequency of the external oscillator is 100 MHz, the first common frequency that can be used for re-clocking is 500 MHz.

\paragraph{Lock Sweep the external oscillator}
\label{sec:lock_sweep}
\indent
\\
\indent
The \textquote{clean} low phase noise high precision external oscillator output can be used directly as timing signal instead of using the WR timing signal (usually 10 MHz) which is generated by the WR implementation on the FPGA.
When the external oscillator can lock at multiple phase offsets (see section  ~\ref{sec:BabyWR_External} above) then it must be taken care of to make sure that it always locks on one single specific phase offset.
The phase offset between the external oscillator and the WR reference clock can be measured once WR determined the proper 1PPS clock tick that is aligned with the timeTransmitter clock.
If this phase offset is other than the single specific phase offset foreseen then the external oscillator is forced in another phase offset position.
This process, called \textquote{Lock Sweep}, is repeated until the external oscillator locked at the single specific phase offset.
The Lock Sweep procedure can be time consuming, depending on the number of possible phase offsets, but it only needs to be performed once after startup or link restart.

\newpage