\section{BabyWR Gateware}

\subsection{HDL synthesis}
Before running the synthesis process you have to make sure your environment is set up correctly.
You will need Vivado 2022.2 (or later) from your Xilinx vendor.

\subsubsection{Downloading the hdl sources}
\label{sec:downloading_the_hdl_sources}
The BabyWR sources are contained in the BabyWR git repository~\cite{babywr.git}.
The sources are based on submodule wr-cores.
Clone the repository and checkout the proper branch:
\begin{lstlisting}[frame=single]
	git clone https://ohwr.org/project/babywr.git
	cd babywr
	git checkout main
	git submodule init
	git submodule update
	cd hdl/wr-cores
	git submodule init
	git submodule update
\end{lstlisting}

Repository wr-cores.git also contains submodules.
By default submodule init and submodule update checkout all the submodules, however, ip\_cores/general-cores and ip\_cores/urv-core should at least be present since BAbyWR depends on them.

Usualy the \emph{hdlmake} tool is used to gather the sources needed to build the firmware. 
Unfortunately \emph{hdlmake} does not support \emph{.tcl}, "Block Memory Mapping" (\emph{.bmm}) or Memory Mapping Information (\emph{.mmi}) files.
The \emph{.bmm} and \emph{.mmi} files are needed to merge software into the FPGA configuration bitfile without the need for re-synthesis of the firmware.
Tcl scripting is used to automatize the build process.

Synthesis is started in the corresponding project \emph{syn} directory.
It is important to source the script from the project/syn directory because it contains the project specific files:
\begin{itemize}
	\item \emph{proj\_properties.tcl}: project specific settings (see Table ~\ref{tab:project_properties})
	\item \emph{proj\_file\_list.txt}: list of files needed by the project (i.e. the output of \emph{hdlmake list-files} plus a few files that are not supported by \emph{hdlmake}.)
\end{itemize}

\begin{table}[H]
	\begin{center}
		\begin{tblr}{|c|c|c|}
			\hline option & default \\ 
			\hline BabyWR version & \textquote{2260-D6-M}  \\ 
			\hline FPGA device & XCAU10P-SBVB484-1  \\ 
			\hline Reference Clock frequency & \textquote{125MHz}  \\ 
			\hline Reference Oscillator & \textquote{local}  \\ 
			\hline 
		\end{tblr}
		\caption{Default options in proj\_properties.tcl. Don't change! (unless you know what you are doing).}
		\label{tab:project_properties}
	\end{center}
\end{table}

\subsubsection{Building the BabyWR reference design}
\label{sec:building_babywr_refdesign}
To build the firmware for the reference design:

\begin{lstlisting}[frame=single]
%cd hdl\syn\babywr_ref_design
\end{lstlisting}

Start Vivado and source the tcl script.

\begin{lstlisting}[frame=single]
vivado
%source ..\..\..\sw\scripts\viv_do_all.tcl 
\end{lstlisting}

Synthesis and bitfile generation may take quite some time.
If the process is successful then you will end up with:
\begin{itemize}
	\item \emph{babywr\_ref\_2260-D6-M\_au10p\_YYDDMM\_HHMM.bit}	
	\item \emph{babywr\_ref\_2260-D6-M\_au10p\_YYDDMM\_HHMM.mmi}
	\item \emph{babywr\_ref\_2260-D6-M\_au10p\_YYDDMM\_HHMM.log}	
	\item \emph{babywr\_ref\_2260-D6-M\_au10p\_YYDDMM\_HHMM\_elf.bit}	
	\item \emph{babywr\_ref\_2260-D6-M\_au10p\_YYDDMM\_HHMM\_elf.mcs}	
	\item \emph{babywr\_ref\_2260-D6-M\_au10p\_YYDDMM\_HHMM\_elf.bin}	
\end{itemize}
in the syn directory, where YYMMDD\_HHMM is the build date and time.
The \emph{.log} file contains the SHA codes from the repositories and sub-modules that were used for the build.
The \emph{.mcs} and \emph{.bin} files can be used to program the FLASH memory of the BabyWR module as described in section~\ref{sec:program flash}.
	
\subsection{Building software}
The \emph{\_elf.bit}, \emph{\_elf.mcs} and \emph{\_elf.bin} files that are created following the instructions described in~\ref{sec:downloading_the_hdl_sources} and ~\ref{sec:building_babywr_refdesign} already contain pre-compiled software (see babywr.git subdirectory: \emph{sw\textbackslash{}precompiled\textbackslash{}wrps-sw-v5\_babywr\textbackslash{}}) for the embedded RISC-V processor, so there is no need to build software unless other embedded software is to be used.

If other embedded software is to be used then follow the instructions in the sections below~\ref{risc_v_compiler} and ~\ref{downloading_c_sources}.
Section~\ref{sec:merge_bit_elf} explains how the software and firmware can be merged into a gateware file.

\subsubsection{RISC-V compiler}
\label{risc_v_compiler}
Before running the compiling the sources you have to make sure your environment is set up correctly.
You will need a RISC-V compiler that runs on a Ubuntu (22.04 LTS) machine.
Information about RISC-V toolchain can be found here~\cite{riscv_toolchain}.

\subsubsection{Downloading C-sources}
\label{downloading_c_sources}
The C-sources for the embedded RISC-V processor in the WR PTP Core~\cite{wrpc} are contained in the wrpc-sw.git~\cite{wrpc-sw.git} repository.
Clone the repository and checkout the proper branch:
\begin{lstlisting}[frame=single]
	git clone https://ohwr.org/project/wrpc-sw.git
	cd wrpc-sw
	git checkout babywr_main
	git submodule init
	git submodule update
\end{lstlisting}
\noindent
Compile the BabyWR sources:
\begin{lstlisting}[frame=single]
	make babywr_defconfig
	make
\end{lstlisting}
Successful compilation results in:
\begin{itemize}
	\item \emph{wrc.elf}	
\end{itemize}

\subsection{Merging .bit and .elf files}
\label{sec:merge_bit_elf}
The Vivado installation contains an \emph{updatemem} command.
An FPGA configuration \emph{.bit} file that contains a memory space (like the RISC-V memory embedded in wr-cores) can be updated with new memory content (i.e. executable software) using \emph{updatemem}.
In order to be able to do this, \emph{updatemem} needs information to locate all BRAM blocks that build up the memory space.
This information is provided by the generated Memory Mapping Information \emph{.mmi} file.
One can merge new compiled software in Executable Loader Format \emph{.elf} with the \emph{.bit} without the need for time consuming re-synthesis.
A script is available to merge an \emph{.elf} file with a \emph{.bit} and \emph{.mmi} file using \emph{updatemem}.

First get into the project directory; the location of the new build \emph{.bit} and \emph{.mmi}.
\begin{lstlisting}[frame=single]
cd ..
\end{lstlisting}
next
\begin{lstlisting}[frame=single]
..\..\sw\scripts\do_vivado_mmi_elf.cmd <bitfile>.bit <elffile>.elf
\end{lstlisting}
which will generate a new \emph{\_elf.bit} that contains the new with Executable Loader Format \emph{.elf} file\footnote{Note: the \emph{.mcs} and \emph{.mcs} will not be updated!} which can be downloaded into the FPGA via the Xilinx JTAG download cable that is plugged into the connector on the carrier board that carries the BabyWR Module.

\subsubsection{Programming the BabyWR FLASH memory}
\label{sec:program flash}
BabyWR contains a 128 Mbit FLASH memory (MT25QU128ABA1EW7-0SIT).
A stable FPGA configuration can be programmed into the FLASH memory of BabyWR such that the module gets configured after Power Up.

Connect the Xilinx JTAG download cable to connector on the carrier board that carries the BabyWR Module.
Start Vivado and select \textquote{Open Hardware Manager}.
Click on \textquote{Open target} and select \textquote{Auto Connect}.
The \textquote{xcau10p} device should be visible as shown in figure ~\ref{fig:hardware_manager}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{./pictures/Other/HardwareManager.png}
	\caption{Vivado Hardware Manager.}
	\label{fig:hardware_manager}
\end{figure}
\noindent
Right click on \textquote{xcau10p} and select \textquote{Add Configuration Memory Device}.
Figure ~\ref{fig:program_flash_1} shows how to select the proper FLASH device.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{./pictures/Other/ProgramFlash_1.png}
	\caption{Select the proper Micron 128 Mbit FLASH device.}
	\label{fig:program_flash_1}
\end{figure}
\noindent
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{./pictures/Other/HardwareManagerFlash.png}
	\caption{XCAU10P with the proper Micron 128 Mbit FLASH device.}
	\label{fig:hardware_manager_flash}
\end{figure}
\noindent
Right click on \textquote{mt25qu128-spi-x1\_x2\_x4} and select \textquote{Program Configuration Memory Device} (see figure ~\ref{fig:program_flash_2}), select the appropriate \emph{.bin}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{./pictures/Other/ProgramFlash_2.png}
	\caption{Select the proper \emph{.bin} configuration file to be programmed. Note: the shown \emph{.bin} Configuration file  file is just an example.}
	\label{fig:program_flash_2}
\end{figure}
\noindent

\newpage