\section{Measurements}

\subsection{Phase noise}
One of the designs goals for BabyWR was to design a module that produces low phase noise clocks.
Low phase noise starts with using a good quality low phase noise local reference oscillator.
BabyWR uses Digital Control Temperature Compensated MEMS Oscillators (DCTCXO, SiTime SIT5359 ~\cite{SiT5359}) which have excellent thermal stability and phase noise specifications.
Figure ~\ref{fig:phase_noise} shows the performance of a 100 MHz SiTime oscillator.

Phase noise power levels (in dB) will increase when multiplying and decrease when dividing frequency, according to equation~\eqref{eq:phasenoise_mul_div} where N is the multiplication factor.

\begin{equation}
	20*log_{10}(N)
	\label{eq:phasenoise_mul_div}
\end{equation}

The 100 MHz SiTime trace is normalized to 10 MHz by subtracting 20 dB (dashed trace) using equation ~\eqref{eq:phasenoise_mul_div} above.
The dashed trace is no more than an indication in order to be able to compare with the 10 MHz clock output of BabyWR.
The white noise floor of the dashed trace is probably higher than shown in the figure.

\begin{figure}[H]
	\centering
%\includegraphics[width=0.8\textwidth]{./pictures/Measurements/PhaseNoise/BabyWR_Sn5_10MHz_gs2kp600ki2_sit5359_100MHz.png}
	\includegraphics[width=0.8\textwidth]{./pictures/Measurements/PhaseNoise/BabyWRv2_Sn3_kp1800ki25.png}
	\caption{BabyWR Phase Noise performance.}
	\label{fig:phase_noise}
\end{figure}

The 10 MHz BabyWR phase noise performance was measured by mounting a BabyWR module on a BabyWr-Carrier board.
BabyWR was brought into synchronization with a low-jitter White Rabbit Switch (WRS-3-LJ/18 running v6.1) in Grand-Master mode.
The BabyWR carrier board SMA connectors J18 and J19 output the LVDS 10 MHz signal.
\textbf{Note that these signals are DC coupled}, so two \textquote{SMA DC-Blocks} (MiniCircuit BLK-18-S+ ~\cite{BLK18}) are used to decouple from the LVDS 1.2 V DC offset.
Behind the DC-Blocks, J18 was coupled to the input of a Rohde\&Schwarz Phase Noise analyzer (FSWP ~\cite{RohdeSchwarz_fswp}) while J19 was terminated into 50 ohm.

The PLL loop bandwidth is a few Hz ($Kp=-1800$, $Ki=-25$) so below $\approx$10 Hz offset the WR switch is in control while above this offset the local oscillator takes over.
At 10 Hz offset it approaches the expected local oscillator noise floor (-102 dBc/Hz).
However, soon at higher offset frequencies, the added phase noise of the FPGA becomes the dominant factor.

The current PLL parameters were chosen without performing extensive optimization.
It is expected that future optimization might improve phase noise slightly.

\subsection{Restart accuracy}
The White Rabbit link restart performance was measured in an automated setup using a python script.
A BabyWR module, mounted on a BabyWR-Carrier board, is put in a climate chamber.
A 1 meter fiber connects to a low-jitter White Rabbit Switch (WRS-3-LJ/18 running v6.1) in Grand-Master mode that is located outside the climate chamber.
No calibration (Tx, Rx or delayCoefficient $\alpha$) was performed since only PPS skew is of interest.
The python script \footnote{For script inspiration see the BabyWR git repository~\cite{babywr.git} \emph{sw/test\_scripts/}} forces a BabyWR reset (using a Agilent 33600A Waveform generator), waits for BabyWR to reach \textquote{TRACK\_PHASE} by reading WR GUI via the USB connection.
Once \textquote{TRACK\_PHASE} is reached, ten samples of the PPS skew between the WR switch PPS and the BabyWR PPS outputs are recorded and averaged using a Time Interval Counter (Agilent 53230A).
Next, the measurement is started all over by a BabyWR reset which, once again, forces a complete White Rabbit link restart.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{./pictures/Measurements/LinkRestart/ConstTemp23_5.png}
	\caption{BabyWR WR link restart.}
	\label{fig:LinkRestart_23degrees}
\end{figure}

The resulting PPS residual is plotted over the White Rabbit link restarts.
Figure ~\ref{fig:LinkRestart_23degrees} shows residual PPS skew at a constant temperature of 23.5\textcelsius{}.
The measured standard deviation over 1360 link restarts is $\approx$ 6 ps while the maximum spread was within 47 ps.

\subsection{Temperature dependency}
The temperature dependency of the White Rabbit time stability was measured.
A BabyWR module, mounted on a BabyWR-Carrier board, is put in a climate chamber.
A 8 km fiber spool (also in the climate chamber), connects to a low-jitter White Rabbit Switch (WRS-3-LJ/18 running v6.1) in Grand-Master mode that is located outside the climate chamber.
The climate chamber temperature follows a profile (ramping at 1\textcelsius{} per minute): 23, 0, 70, 0, 23 \textcelsius{}.
The White Rabbit PPS skew measurements were performance in an automated setup using a python script.
The python script \footnote{For script inspiration see the BabyWR git repository~\cite{babywr.git} \emph{sw/test\_scripts/}} waits for BabyWR to reach \textquote{TRACK\_PHASE} by reading WR GUI via the USB connection.
Once \textquote{TRACK\_PHASE} is reached, repeatedly, ten samples of the PPS skew between the WR switch PPS and the BabyWR PPS outputs are recorded and averaged using a Time Interval Counter (Agilent 53230A).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{./pictures/Measurements/Skew/Skew_xadc_0_70.png}
	\caption{BabyWR PPS skew temperature dependency.}
	\label{fig:Skew_xadc_0_70}
\end{figure}

The upper graph of figure~\ref{fig:Skew_xadc_0_70} shows residual PPS skew.
Conclusions follow after describing the other two graphs.

The middle graph of figure~\ref{fig:Skew_xadc_0_70} shows the measured cable round trip time (CRTT).
The delay of the 8 km fiber is temperature depend.
The measured spread over 70\textcelsius{} is in the order of 22 ns.
The fiber spool temperature lags behind due to its thermal capacitance.

The bottom graph of figure~\ref{fig:Skew_xadc_0_70} shows the FPGA die temperature that was measured using the FPGA XADC.
The maximum FPGA temperature is in the order of 27\textcelsius{} above ambient, with airflow due to the climate chamber fan.

A few conclusions can be drawn from the three graphs of figure ~\ref{fig:Skew_xadc_0_70}.
The PPS skew measurement (upper graph) almost perfectly follows the ambient temperature.
However, due to the delayed response of the fiber spool because of its thermal capacitance, one can see that the fiber delayCoefficient ($\alpha$) is also temperature dependent which causes the PPS skew measurement to divert from the ambient temperature curve. 
The main PPS skew temperature dependency is due to the BabyWR module itself (i.e., the FPGA routing delays are temperature dependent).
The BabyWR has a thermal coefficient of 6.3 ps per \textcelsius{} ($\approx$ 444 ps / 70\textcelsius{} see upper graph of figure ~\ref{fig:Skew_xadc_0_70}).
Improvement seems possible with a first order compensation that can be implemented by reading the FPGA temperature and adjusting the calibration parameters (not yet part of the current design).

\subsection{Power supply sequence}
The FPGA on BabyWR needs several voltages: VCCINT \& VCCBRAM (0.85V), Gigabit Transceiver voltages (0.9V and 1.2 V), VCCAUX \& VCCO (1.8V) and VCCO (3V3).
The input supply voltage for BabyWR is 3.3V.
An on board Silent Switcher $\mu$Module Regulator (LTM8051 ~\cite{LTM8051}) creates 0.85V, 0.9V, 1.2V and 1.8V.
Figure ~\ref{fig:PowerUpLTM8051} shows the power on sequence of these voltages.
Bank 84 and 85 of the FPGA are powered with 3.3V.
However, the power supply sequence should be such that 3.3V on VCCO is the last supply voltage that should become active.
This sequence is accomplished by a switching FET that forwards the 3.3V power supply to \textquote{3.3V switched} (3V3sw) when the \textquote{Power Good} signal of the $\mu$Module Regulator LTM8051 is asserted.
Figure ~\ref{fig:PowerUp3V3} shows the 3.3V input voltage in relation to the 1.2V and 1.8V $\mu$Module Regulator LTM8051 voltages and 3.3V switched that is turned on after \textquote{Power Good} and a small delay.

The respective power down sequences are shown in Figures ~\ref{fig:PowerDownLTM8051} and ~\ref{fig:PowerDown3V3}.
The power supply sequences are complaint with the Xilinx recommendation (see ~\cite{DS931}, \textquote{Power-On/Off Power Supply Sequencing}).

\begin{figure}[ht]
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./pictures/Measurements/PowerSequence/PowerUpSequenceLTM8051.png}
		\caption{LTM8051 voltages}
		\label{fig:PowerUpLTM8051}
	\end{minipage}
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./pictures/Measurements/PowerSequence/PowerUpSequence3V3sw.png}
		\caption{In relation to 3V3 and 3V3sw.}
		\label{fig:PowerUp3V3}
	\end{minipage}
	\hspace{0.5cm}
\end{figure}

\begin{figure}[ht]
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./pictures/Measurements/PowerSequence/PowerDownSequenceLTM8051.png}
		\caption{LTM8051 voltages}
		\label{fig:PowerDownLTM8051}
	\end{minipage}
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./pictures/Measurements/PowerSequence/PowerDownSequence3V3sw.png}
		\caption{In relation to 3V3 and 3V3sw.}
		\label{fig:PowerDown3V3}
	\end{minipage}
	\hspace{0.5cm}
\end{figure}

\subsection{PCIe waveforms}
A BabyWR module, mounted on a BabyWr-Carrier board, was placed in the \textquote{PCIe 3.0 Compliance Base Board (CBB3.0) test fixture for testing PCI Express Add-in Cards} ~\cite{CBB3.0}.

Figure ~\ref{fig:PCIe_2.5GTs} shows the PCIe signal waveforms for 2.5 GT/s transfers.
Figure ~\ref{fig:PCIe_5GTs} shows the PCIe signal waveforms for 5 GT/s transfers.

\begin{figure}[ht]
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./pictures/Measurements/PCIe/2_5aGT.png}
		\caption{2.5 GT/s PCIe waveform.}
		\label{fig:PCIe_2.5GTs}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./pictures/Measurements/PCIe/5aGT.png}
		\caption{5 GT/s PCIe waveform.}
		\label{fig:PCIe_5GTs}
	\end{minipage}
\end{figure}


\newpage