#!/bin/bash

# convert all commandline arguments that end in .svg to .pdf files using inkscape

for f in *.svg
do
  echo "Converting $f to pdf"
  inkscape $f --export-area-drawing --export-type=pdf
  inkscape $f --export-area-drawing --export-type=png
done

