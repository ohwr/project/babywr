@prompt $$$s

rem inkscape version 1.3.2 and higher
rem convert all commandline arguments that end in .svg to .pdf files using inkscape

for %%a in (*.svg) do call :do_convert %%a
goto donext

:do_convert
   echo "Converting %1 to pdf"
   inkscape %1 --export-area-drawing --export-type=pdf
   inkscape %1 --export-area-drawing --export-type=png
   goto :eof

:donext
